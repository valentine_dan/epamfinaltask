use epammusicdb;
drop procedure if exists buyTrack;
create procedure buyTrack(userId INTEGER, trackId INTEGER, paymentDate DATE, paymentNumb INTEGER)
begin
  insert into usertrack (user_id, track_id, date, paymentNumber) values (userId, trackId, paymentDate, paymentNumb);
end;