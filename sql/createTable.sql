USE `epamMusicDB`;

CREATE TABLE `Performer`
(
  `id`   INTEGER      NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL unique,
  PRIMARY KEY (`id`)
) engine = innodb
  default character set utf8;



CREATE TABLE `Album`
(
  `id`           INTEGER      NOT NULL AUTO_INCREMENT,
  `name`         VARCHAR(255) NOT NULL,
  `year`         INTEGER      NOT NULL,
  `performer_id` INTEGER      NOT NULL,
  `cover`        VARCHAR(255) DEFAULT '/img/default.jpg',
  PRIMARY KEY (`id`),
  FOREIGN KEY (performer_id)
    REFERENCES `Performer` (id)
    on delete restrict
    on update cascade
) engine = innodb
  default character set utf8;

CREATE TABLE `Track`
(
  `id`       INTEGER       NOT NULL AUTO_INCREMENT,
  `name`     VARCHAR(255)  NOT NULL,
  `price`    DECIMAL(19,2) NOT NULL,
  `album_id` INTEGER       NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`album_id`)
    REFERENCES `Album` (id)
    on delete restrict
) engine = innodb
  default character set utf8;

CREATE TABLE `Playlist`
(
  `id`    INTEGER      NOT NULL AUTO_INCREMENT,
  `name`  varchar(255) NOT NULL,
  `genre` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) engine = innodb
  default character set utf8;

CREATE TABLE `TrackPlaylist`
(
  `track_id`    INTEGER NOT NULL,
  `playlist_id` INTEGER NOT NULL,
  FOREIGN KEY (`track_id`)
    REFERENCES `Track` (`id`)
    on delete cascade
    on update cascade,
  FOREIGN KEY (`playlist_id`)
    REFERENCES `Playlist` (`id`)
    on delete cascade
    on update cascade
) engine = innodb
  default character set utf8;

CREATE TABLE `BankAccount`
(
  `id`             INTEGER       NOT NULL AUTO_INCREMENT,
  `number`         INTEGER       NOT NULL,
  `availableMoney` DECIMAL(19,2) NOT NULL DEFAULT 0.0000,
  PRIMARY KEY (`id`)
) engine innodb
  default character set utf8;

CREATE TABLE `User`
(
  `id`             INTEGER       NOT NULL AUTO_INCREMENT,
  `login`          VARCHAR(255)  NOT NULL UNIQUE,
  `password`       VARCHAR(50)   NOT NULL,
  `role`           TINYINT       NOT NULL CHECK (`role` IN (0, 1)),
  `name`           VARCHAR(255)  NOT NULL,
  `surname`        VARCHAR(255)  NOT NULL,
  `email`          varchar(255)  not null,
  `bankAccount_id` INTEGER,
  `moneyAmount`    DECIMAL(19,2) not null default 0,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`bankAccount_id`)
    references BankAccount (`id`)
    on update cascade
) engine = innodb
  default character set utf8;



CREATE TABLE `UserTrack`
(
  `user_id`       INTEGER  NOT NULL,
  `track_id`      INTEGER  NOT NULL,
  `date`          DATETIME NOT NULL,
  `paymentNumber` INTEGER  NOT NULL,
  FOREIGN KEY (`user_id`)
    REFERENCES `User` (`id`)
    on delete cascade
    on update cascade,
  FOREIGN KEY (`track_id`)
    REFERENCES `Track` (`id`)
    on delete restrict
    on update cascade
) engine = innodb
  default character set utf8;

create table `Bonus`
(
  `id`    integer                         not null auto_increment,
  `value` double                          not null default 0,
  `type`  set ('DISCOUNT', 'FREE_TRACK') not null,
  primary key (id)
) engine = innodb
  default character set utf8;

create table `UserBonus`
(
  `bonus_id` integer not null,
  `user_id`  integer not null,
  foreign key (user_id)
    references User (id)
    on delete cascade
    on update cascade,
  foreign key (bonus_id)
    references Bonus (id)
    on delete cascade
    on update cascade
) engine = innodb
  default character set utf8;




