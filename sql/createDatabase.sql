CREATE DATABASE if not exists `epamMusicDB` DEFAULT CHARACTER SET utf8;
create user if not exists 'admin'@'localhost' identified by '1623107';

GRANT SELECT, INSERT, UPDATE, DELETE, EXECUTE
ON `epamMusicDB`.*
TO admin@localhost;