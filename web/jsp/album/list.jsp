<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="u" %>

<u:html user="${user}" title="Albums - user - ${user.getLogin()}">
  <div class="container">
    <div class="row">
      <div class="col-md-2 col-lg-2"></div>
      <div class="col-md-8 col-lg-8 text-center">
        <h2>List of albums you can purchase on our web-site</h2>
        <c:if test="${user.getRole().getIdentity() eq 0}">
          <button type="button" class="btn" data-toggle="modal" data-target="#album-add-modal">
            Add album
          </button>
          <u:modal type="album-add" title="Adding album" url="/album/add.html">
            <label for="performer">Select performer</label>
            <select id="performer" name="performer">
              <c:choose>
                <c:when test="${not empty performers}">
                  <c:forEach items="${performers}" var="performer">
                    <option value="${performer.getId()}">${performer.getName()}</option>
                  </c:forEach>
                </c:when>
                <c:otherwise>
                  <p>List of performers is empty</p>
                </c:otherwise>
              </c:choose>
            </select>
            <label for="name">Enter album name:</label>
            <input class="form-control" type="text" name="albumName" id="name">
            <label for="year">Enter album year:</label>
            <input class="form-control" type="text" name="albumYear" id="year">
            <div class="modal-footer">
              <button type="submit" class="btn">Add</button>
              <button type="reset" class="btn" data-dismiss="modal">Close</button>
            </div>
          </u:modal>
        </c:if>
          <%--Modal for error message--%>
        <u:modal type="error" title="Error" msg="${message}"/>
        <c:url value="/album/edit.html" var="albumEditUrl"/>
        <c:url value="/album/delete.html" var="albumDeleteUrl"/>
        <table class="table box-shadow">
          <thead>
          <tr>
            <th>#</th>
            <th>Album name</th>
            <th>Year</th>
            <th>Performer</th>
            <th></th>
          </tr>
          </thead>
          <tbody>
          <c:forEach items="${albums}" var="album" varStatus="i"
                     begin="${(currentPage-1)*(itemsPerPage-1)}"
                     end="${currentPage*(itemsPerPage-1)}">
            <tr>
              <td class="align-middle">
                  ${i.count+(i.begin)}
              </td>
              <td class="align-middle">
                  ${album.getName()}
              </td>
              <td class="align-middle">${album.getYear()}</td>
              <td class="align-middle">
                  ${album.getPerformer().getName()}
              </td>
              <td class="align-middle">
                <u:buttons type="album" entity="${album}" user="${user}"/>
              </td>
            </tr>
          </c:forEach>
          </tbody>
        </table>
      </div>
      <div class="col-md-2 col-lg-2"></div>
    </div>
    <div class="d-flex justify-content-center">
      <c:url value="/album/list.html?page=${currentPage-1}" var="prevPageUrl"/>
      <c:url value="/album/list.html?page=${currentPage+1}" var="nextPageUrl"/>
      <ul class="pagination">
        <c:choose>
          <c:when test="${(currentPage eq 1)}">
            <li class="page-item disabled"><a class="page-link" href="${prevPageUrl}">Prev</a></li>
          </c:when>
          <c:otherwise>
            <li class="page-item"><a class="page-link" href="${prevPageUrl}">Prev</a></li>
          </c:otherwise>
        </c:choose>
        <li class="page-item disabled"><a class="page-link" href="">${currentPage}</a></li>
        <c:choose>
          <c:when test="${currentPage eq numberOfPages}">
            <li class="page-item disabled"><a class="page-link" href="${nextPageUrl}">Next</a></li>
          </c:when>
          <c:otherwise>
            <li class="page-item"><a class="page-link" href="${nextPageUrl}">Next</a></li>
          </c:otherwise>
        </c:choose>
      </ul>
    </div>
  </div>
</u:html>
