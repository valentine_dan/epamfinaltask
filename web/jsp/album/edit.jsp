<%@ taglib tagdir="/WEB-INF/tags" prefix="u" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<u:html user="${user}" title="Admin - album editing - ${album.getName()}">
  <div class="container">
    <c:url value="/album/save.html" var="albumSaveUrl"/>
    <form class="form" action="${albumSaveUrl}" method="post">
      <label for="name">Enter new name</label>
      <input class="form-control" type="text" id="name" name="name" minlength="1" value="${album.getName()}" required>
      <label for="year">Enter new year</label>
      <input class="form-control" type="number" id="year" name="year" minlength="4" value="${album.getYear()}" required>
      <label for="performer">Select new performer</label>
      <select class="form-control" id="performer" name="performer" required>
        <c:choose>
          <c:when test="${not empty performers}">
            <c:forEach items="${performers}" var="performer">
              <option value="${performer.getId()}">${performer.getName()}</option>
            </c:forEach>
          </c:when>
          <c:otherwise>
            <p>List of performers is empty</p>
          </c:otherwise>
        </c:choose>
      </select>
      <input type="hidden" name="id" value="${album.getId()}">
      <button class="btn" type="submit">Save</button>
      <button class="btn" type="reset">Reset</button>
    </form>
  </div>
</u:html>
