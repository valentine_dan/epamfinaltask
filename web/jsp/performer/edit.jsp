<%@ taglib tagdir="/WEB-INF/tags" prefix="u" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<u:html user="${user}" title="Admin - performer editing - ${performer.getName()}">
  <div class="container">
    <c:url value="/performer/save.html" var="albumSaveUrl"/>
    <form class="form" action="${albumSaveUrl}" method="post">
      <label for="name">Performer name</label>
      <input class="form-control" type="text" id="name" name="name" minlength="1" value="${performer.getName()}" required>
      <input class="form-control" type="hidden" name="id" value="${performer.getId()}">
      <button class="btn" type="submit">Save</button>
      <button class="btn" type="reset">Reset</button>
    </form>
  </div>
</u:html>
