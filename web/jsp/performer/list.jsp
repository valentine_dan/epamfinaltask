<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="u" %>

<u:html user="${user}" title="Performers - user -${user.getLogin()}">
  <div class="container">
    <div class="row">
      <div class="col-md-3 col-lg-3"></div>
      <div class="col-md-6 col-lg-6 text-center">
        <h3>List of performers</h3>
        <c:if test="${user.getRole().getIdentity() eq 0}">
          <button type="button" class="btn" data-toggle="modal" data-target="#performer-add-modal">
            Add performer
          </button>
          <%--Modal for adding track--%>
          <u:modal type="performer-add" title="Adding performer" url="/performer/add.html">
            <label for="name">Enter performer name:</label>
            <input class="form-control" type="text" name="performerName" id="name">
            <div class="modal-footer">
              <button type="submit" class="btn">Add</button>
              <button type="reset" class="btn" data-dismiss="modal">Close</button>
            </div>
          </u:modal>
          <%--Modal for error message--%>
          <u:modal type="error" title="Error" msg="${message}"/>
        </c:if>
        <c:url value="/performer/edit.html" var="albumEditUrl"/>
        <c:url value="/performer/delete.html" var="albumDeleteUrl"/>
        <table class="table box-shadow" style="max-width: 500px">
          <thead>
          <tr>
            <th>#</th>
            <th>Performer</th>
          </tr>
          </thead>
          <tbody>
          <c:forEach items="${performers}" var="performer" varStatus="i"
                     begin="${(currentPage-1)*(itemsPerPage-1)}"
                     end="${currentPage*(itemsPerPage-1)}">
            <tr>
              <td class="align-middle">
                  ${i.count+(i.begin)}
              </td >
              <td class="align-middle">
                <c:url value="/track/list.html?performerId=${performer.getId()}" var="tracksByPerfId"/>
                <a href="${tracksByPerfId}">${performer.getName()}</a>
              </td>
            </tr>
          </c:forEach>
          </tbody>
        </table>
      </div>
      <div class="col-md-3 col-lg-3"></div>
    </div>
    <div class="d-flex justify-content-center">
      <c:url value="/performer/list.html?page=${currentPage-1}" var="prevPageUrl"/>
      <c:url value="/performer/list.html?page=${currentPage+1}" var="nextPageUrl"/>
      <ul class="pagination">
        <c:choose>
          <c:when test="${(currentPage eq 1)}">
            <li class="page-item disabled"><a class="page-link" href="${prevPageUrl}">Prev</a></li>
          </c:when>
          <c:otherwise>
            <li class="page-item"><a class="page-link" href="${prevPageUrl}">Prev</a></li>
          </c:otherwise>
        </c:choose>
        <li class="page-item disabled"><a class="page-link" href="">${currentPage}</a></li>
        <c:choose>
          <c:when test="${currentPage eq numberOfPages}">
            <li class="page-item disabled"><a class="page-link" href="${nextPageUrl}">Next</a></li>
          </c:when>
          <c:otherwise>
            <li class="page-item"><a class="page-link" href="${nextPageUrl}">Next</a></li>
          </c:otherwise>
        </c:choose>
      </ul>
    </div>
  </div>
</u:html>
