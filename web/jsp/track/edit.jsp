<%@ taglib tagdir="/WEB-INF/tags" prefix="u" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<u:html user="${user}" title="Admin - track editing - ${track.getName()}">
  <div class="container">
    <c:url value="/track/save.html" var="albumSaveUrl"/>
    <form class="form" action="${albumSaveUrl}" method="post">
      <label for="name">Track name</label>
      <input class="form-control" type="text" id="name" name="name" minlength="1" value="${track.getName()}">
      <label for="price">Track price</label>
      <input class="form-control" type="number" id="price" name="price" min="0" value="${track.getPrice()}">
      <label for="albm">Album</label>
      <select class="form-control" id="albm" name="album">
        <c:choose>
          <c:when test="${not empty albums}">
            <c:forEach items="${albums}" var="album">
              <c:choose>
                <c:when test="${track.getAlbum().getName() eq album.getName()}">
                  <option selected value="${album.getId()}">${album.getName()}</option>
                </c:when>
                <c:otherwise>
                  <option value="${album.getId()}">${album.getName()} - ${album.getPerformer().getName()}</option>
                </c:otherwise>
              </c:choose>
            </c:forEach>
          </c:when>
          <c:otherwise>
            <p>List of albums is empty</p>
          </c:otherwise>
        </c:choose>
      </select>
      <input type="hidden" name="id" value="${track.getId()}">
      <button type="submit" class="btn">Save</button>
      <button type="reset" class="btn">Reset</button>
    </form>
  </div>
</u:html>
