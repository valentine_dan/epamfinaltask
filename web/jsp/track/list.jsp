<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="u" %>

<u:html user="${user}" title="Tracks - user -${user.getLogin()}">
    <div class="container">
      <div class="row">
        <div class="col-md-2 col-lg-2"></div>
        <div class="col-md-8 col-lg-8 text-center">
          <h2>List of tracks you can purchase on our web-site</h2>
          <c:if test="${user.getRole().getIdentity() eq 0}">
            <button type="button" class="btn" data-toggle="modal" data-target="#track-add-modal">
              Add track
            </button>
            <%--Modal for adding track--%>
            <u:modal type="track-add" title="Adding track" url="/track/add.html">
              <div class="modal-body">
                <label for="albm">Select album:</label>
                <select class="form-control" id="albm" name="album">
                  <c:choose>
                    <c:when test="${not empty albums}">
                      <c:forEach items="${albums}" var="album">
                        <option value="${album.getId()}">
                            ${album.getName()} - ${album.getPerformer().getName()}
                        </option>
                      </c:forEach>
                    </c:when>
                    <c:otherwise>
                      <p>List of albums is empty</p>
                    </c:otherwise>
                  </c:choose>
                </select>
                <label for="name">Enter track name:</label>
                <input class="form-control" type="text" name="trackName" id="name">
                <label for="price">Enter track price:</label>
                <input class="form-control" type="text" name="trackPrice" id="price">
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn">Add</button>
                <button type="reset" class="btn" data-dismiss="modal">Close</button>
              </div>
            </u:modal>
            <%--Modal for error message--%>
            <u:modal type="error" title="Error" msg="${message}"/>
          </c:if>
          <c:url value="/track/edit.html" var="albumEditUrl"/>
          <c:url value="/track/delete.html" var="albumDeleteUrl"/>
          <table class="table box-shadow">
            <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Price</th>
              <th>Performer</th>
              <th></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${tracks}" var="album" varStatus="i"
                       begin="${(currentPage-1)*(itemsPerPage-1)}"
                       end="${currentPage*(itemsPerPage-1)}">
              <tr>
                <td class="align-middle">
                    ${i.count+(i.begin)}
                </td>
                <td class="align-middle">
                    ${album.getName()}
                </td>
                <td class="align-middle">${album.getPrice()}</td>
                <td class="align-middle">${album.getAlbum().getPerformer().getName()}</td>
                <td class="align-middle">
                  <u:buttons type="track" entity="${album}" user="${user}"/>
                </td>
              </tr>
            </c:forEach>
            </tbody>
          </table>
        </div>
        <div class="col-md-2 col-lg-2"></div>
      </div>
        <div class="d-flex justify-content-center">
          <c:url value="/track/list.html?page=${currentPage-1}" var="prevPageUrl"/>
          <c:url value="/track/list.html?page=${currentPage+1}" var="nextPageUrl"/>
          <div class="d-flex justify-content-center">
            <ul class="pagination">
              <c:choose>
                <c:when test="${(currentPage eq 1)}">
                  <li class="page-item disabled"><a class="page-link" href="${prevPageUrl}">Prev</a></li>
                </c:when>
                <c:otherwise>
                  <li class="page-item"><a class="page-link" href="${prevPageUrl}">Prev</a></li>
                </c:otherwise>
              </c:choose>
              <li class="page-item disabled"><a class="page-link" href="">${currentPage}</a></li>
              <c:choose>
                <c:when test="${currentPage eq numberOfPages}">
                  <li class="page-item disabled"><a class="page-link" href="${nextPageUrl}">Next</a></li>
                </c:when>
                <c:otherwise>
                  <li class="page-item"><a class="page-link" href="${nextPageUrl}">Next</a></li>
                </c:otherwise>
              </c:choose>
            </ul>
          </div>
        </div>
      </div>
    </div>
</u:html>


