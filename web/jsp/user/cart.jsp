<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="u" %>

<u:html user="${user}" title="${user.getLogin()} - Cart">
  <div class="px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
    <h1 class="display-4"><i class="fas fa-cart-arrow-down"></i> <strong>Your cart</strong></h1>
    <p class="lead">Here you buy tracks or remove them from cart</p>
    <c:url value="/user/buy.html" var="buyUrl"/>
    <form method="post" class="form" action="${buyUrl}" id="buy-tracks-form">
      <label for="disount">Select your discount</label>
      <select id="disount" name="discount" class="form-control box-shadow">
        <c:forEach items="${user.getBonuses()}" var="bonus">
          <c:if test="${bonus.getBonusType().ordinal() eq 0}">
            <option value="${bonus.getId()}">${bonus.getDiscountValue()}% discount</option>
          </c:if>
        </c:forEach>
      </select>
      <p>Total price: ${user.getCart().getTotalPrice()}</p>
      <button type="submit" class="btn">Buy</button>
    </form>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-lg-6">
        <h2>Tracks in cart</h2>
        <table class="table box-shadow">
          <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Price</th>
            <th></th>
          </tr>
          </thead>
          <tbody>
          <c:forEach items="${user.getCart().getTracks()}" var="track" varStatus="i">
            <tr>
              <td>${i.count}</td>
              <td>${track.getName()} <span
                      style="color: #999999">${track.getAlbum().getPerformer().getName()}</span></td>
              <td>${track.getPrice()}</td>
              <td>
                <c:if test="${not empty freeTracks}">
                  <c:url value="/user/getFreeTrack.html" var="getFreeTrack"/>
                  <form style="display: none" id="form-get-free-track" action="${getFreeTrack}">
                    <input type="hidden" name="id" value="${track.id}">
                  </form>
                  <button class="btn" onclick="sendRequest('form-get-free-track')">
                    Get free
                  </button>
                </c:if>
                <c:url value="/user/getFreeTrack.html" var="getFreeTrack"/>
                <c:url value="/user/trackRemoveFromCart.html" var="removeFromCartUrl"/>
                <form style="display: none" action="${removeFromCartUrl}"
                      id="track-remove-from-cart-form">
                  <input type="hidden" name="id" value="${track.getId()}">
                </form>
                <button type="submit" class="btn"
                        onclick="sendRequest('track-remove-from-cart-form')">
                  Remove from cart
                </button>
              </td>
            </tr>
          </c:forEach>
          </tbody>
        </table>
      </div>
      <div class="col-md-6 col-lg-6">
        <h2>Albums in cart</h2>
        <table class="table">
          <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th></th>
          </tr>
          </thead>
          <tbody>
          <c:forEach items="${user.getCart().getAlbums()}" var="album" varStatus="i">
            <tr>
              <td>${i.count}</td>
              <td>${album.getName()}
                <span style="color: #999999">${album.getPerformer().getName()}</span></td>
              <td>
                <c:url value="/user/albumRemoveFromCart.html" var="removeFromCartUrl"/>
                <form style="display: none" action="${removeFromCartUrl}"
                      id="album-remove-from-cart-form">
                  <input type="hidden" name="id" value="${album.getId()}">
                </form>
                <button type="submit" class="btn"
                        onclick="sendRequest('album-remove-from-cart-form')">
                  Remove from cart
                </button>
              </td>
            </tr>
          </c:forEach>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</u:html>
