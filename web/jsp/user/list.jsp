<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="u" %>

<u:html user="${user}" title="${user.getLogin()} - All users">
  <div class="container">
    <div class="row">
      <div class="col-md-1 col-lg-1"></div>
      <div class="col-md-10 col-lg-10 text-center">
        <h1>Users</h1>
        <table class="table box-shado">
          <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Surname</th>
            <th>E-Mail</th>
            <th>Login</th>
            <th>Balance</th>
            <th></th>
          </tr>
          </thead>
          <tbody>
          <c:forEach items="${users}" var="listUser">
            <c:set value="${listUser.getId()}" var="id"/>
            <tr>
              <td class="align-middle">${id}
                <u:modal type="bonus-add" title="Bonus adding" url="/admin1/addDiscount.html">
                  <div class="modal-body">
                    <label for="discValue">Enter discount value</label>
                    <input type="number" min="1" name="discValue" id="discValue">
                    <input type="hidden" name="id" value="${id}">
                  </div>
                  <div class="modal-footer">
                    <button type="submit" class="btn">Add</button>
                    <button type="reset" class="btn" data-dismiss="modal">Close</button>
                  </div>
                </u:modal>
              </td>
              <td class="align-middle">${listUser.getName()}</td>
              <td class="align-middle">${listUser.getSurname()}</td>
              <td class="align-middle">${listUser.getEmail()}</td>
              <td class="align-middle">${listUser.getLogin()}</td>
              <td class="align-middle">${listUser.getBalance()}</td>
              <td class="align-middle">
                <button class="btn" type="button" data-toggle="modal" data-target="#bonus-add-modal">
                  Add discount
                </button>
                <c:url value="/admin/addFreeTrack.html?userId=${id}" var="freeTrackAddingUrl"/>
                <a class="btn" type="button" href="${freeTrackAddingUrl}">Add free track</a>
              </td>
            </tr>
          </c:forEach>
          </tbody>
        </table>
      </div>
      <div class="col-md-1 col-lg-1"></div>
    </div>
  </div>
</u:html>
