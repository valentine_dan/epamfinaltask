<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="u" %>

<u:html title="${user.getLogin()} - Profile" user="${user}">
  <div class="px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
    <p class="lead">Profile of user</p>
    <h1 class="display-4"><strong>${user.getLogin()}</strong></h1>
  </div>
  <div class="container">
    <div class="card-deck mb-3 text-center">
      <div class="card mb-4 box-shadow">
        <div class="card-header">
          <h4 class="my-0 font-weight-normal">
            General info
          </h4>
        </div>
        <div class="card-body align-middle">
          <ul class="list-unstyled mt-3 mb-4">
            <li>
              Name: ${user.getName()}
            </li>
            <li>
              Surname: ${user.getSurname()}
            </li>
            <li>
              Login: ${user.getLogin()}
            </li>
            <li>
              <span><i class="far fa-envelope-open"></i> ${user.getEmail()}</span>
              <a class="link text-muted" data-toggle="modal"
                 data-target="#email-modal" href="">
                Change E-mail
              </a>
              <ul class="list-unstyled">
                <li>Free tracks - ${freeTracksNum}x </li>
                <c:forEach items="${discounts}" var="discount">
                  <li>
                    Discount on purchase - ${discount.getDiscountValue()}%
                  </li>
                </c:forEach>
              </ul>
              <u:modal type="email" url="/user/emailChanging.html" title="Changing E-Mail">
                <label for="old-email">Enter old E-Mail</label>
                <input type="email" class="form-control" name="old-email" id="old-email" required>
                <label for="new-email">Enter new E-Mail</label>
                <input type="email" class="form-control" name="new-email" id="new-email" required>
                <div class="modal-footer">
                  <button type="submit" class="btn" id="submit-email-change">Change</button>
                  <button type="reset" class="btn" data-dismiss="modal">Close</button>
                </div>
              </u:modal>
            </li>
          </ul>

        </div>
        <div class="card-footer">
          <button type="button" class="btn" data-toggle="modal" data-target="#password-modal">
            Change password
          </button>
          <u:modal type="password" url="/user/passwordChanging.html" title="Changing password">
            <label for="old-password">Enter old password</label>
            <input type="password" class="form-control" name="old-password" id="old-password"
                   pattern="[A-Za-z0-9]([A-Za-z0-9._]{5,23})[A-Za-z0-9]"
                   required>
            <label for="new-password-1">Enter new password</label>
            <input type="password" class="form-control" name="newPass1" id="new-password-1"
                   pattern="[A-Za-z0-9]([A-Za-z0-9._]{5,23})[A-Za-z0-9]"
                   required>
            <label for="new-password-2">Repeat new password</label>
            <input type="password" class="form-control" name="newPass2" id="new-password-2"
                   pattern="[A-Za-z0-9]([A-Za-z0-9._]{5,23})[A-Za-z0-9]"
                   required>
            <div class="modal-footer">
              <button type="submit" class="btn" id="submit-password-change">Change</button>
              <button type="reset" class="btn" data-dismiss="modal">Close</button>
            </div>
          </u:modal>
        </div>
      </div>
      <div class="card mb-4 box-shadow">
        <div class="card-header">
          <h4 class="my-0 font-weight-normal">Payment method</h4>
        </div>
        <c:choose>
        <c:when test="${not empty user.getBankAccount()}">
          <div class="card-body">
            <div class="text-center">
              <ul class="list-unstyled mt-3 mb-4">
                <li>Your bank account number: ${user.getBankAccount().getNumber()}</li>
                <li>Your balance: <span data-feather="dollar-sign"></span> ${user.getBalance()}
                </li>
              </ul>
            </div>
          </div>
          <div class="card-footer">
            <button class="btn" type="button" data-toggle="modal" data-target="#top-up-balance-modal">
              Top up balance
            </button>
          </div>
          <u:modal type="top-up-balance" url="/user/updateBalance.html" title="Adding money">
            <label for="moneyAmount">Enter money amount</label>
            <input type="text" id="moneyAmount" name="moneyAmount" min="0" required>
            <div class="modal-footer">
              <button type="submit" class="btn">Top up</button>
              <button type="reset" class="btn" data-dismiss="modal">Close</button>
            </div>
          </u:modal>
          <u:modal type="info" title="Info" msg="${message}"/>
        </c:when>
        <c:otherwise>
        <div class="card-body">
              <span class="align-middle">
                    You have not added a payment method yet.
                  </span>
        </div>

        <div class="card-footer">
          <button class="btn" type="button" data-toggle="modal" data-target="#payment-method-modal">
            Add payment method
          </button>
        </div>
      </div>
      <u:modal type="payment-method" url="/user/addPaymentMethod.html" title="Adding payment method">
        <label for="number">Enter bank account number</label>
        <input class="form-control" type="text" id="number" name="number">
        <div class="modal-footer">
          <button type="submit" class="btn">Add</button>
          <button type="reset" class="btn" data-dismiss="modal">Close</button>
        </div>
      </u:modal>
      </c:otherwise>
      </c:choose>
    </div>
  </div>
  </div>

  <c:choose>
    <c:when test="${not ownedTracks.isEmpty()}">
      <div class="container">
      <div class="row">
        <div class="col-md-2 col-lg-2"></div>
        <div class="col-md-8 col-lg-8 text-center">
          <h1>Already owned tracks</h1>
          <table class="table box-shadow">
            <thead>
            <tr>
              <th>Name</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${ownedTracks}" var="album">
              <tr>
                <td class="align-middle">
                    ${album.getName()} <span
                        style="color: #999999">${album.getAlbum().getPerformer().getName()}</span>
                </td>
              </tr>
            </c:forEach>
            </tbody>
          </table>
        </div>
        <div class="col-md-2 col-lg-2"></div>
      </div>
    </c:when>
  </c:choose>
  </div>
  </div>
</u:html>