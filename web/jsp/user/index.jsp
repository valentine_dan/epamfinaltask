<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="u" %>

<u:html user="${user}" title="epam music">
  <div class="px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
    <h1 class="display-4"><strong>epam music</strong></h1>
    <p class="lead">Here you can find some music</p>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-md-4 text-center">
        <h1>Tracks</h1>
        <table class="table">
          <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
          </tr>
          </thead>
          <tbody>
          <c:forEach items="${tracks}" var="album" end="7" varStatus="i">
            <tr>
              <td class="align-middle">${i.count}</td>
              <td class="align-middle">
                  ${album.getName()}
                <c:url value="/track/list.html?performerId=${album.getAlbum().getPerformer().getId()}&page=1"
                       var="trackByPerformerListUrl"/>
                <a style="color: #999999" href="${trackByPerformerListUrl}">
                    ${album.getAlbum().getPerformer().getName()}
                </a>
              </td>
            </tr>
          </c:forEach>
          </tbody>
        </table>
      </div>
      <div class="col-md-4 text-center">
        <h1>Albums</h1>
        <table class="table">
          <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
          </tr>
          </thead>
          <tbody>
          <c:forEach items="${albums}" var="album" end="7" varStatus="i">
            <tr>
              <td class="align-middle">${i.count}</td>
              <td class="align-middle">
                  ${album.getName()}
                <c:url value="/album/list.html?id=${album.getPerformer().getId()}&page=1"
                       var="albumByPerformerListUrl"/>
                <a style="color: #999999" href="${albumByPerformerListUrl}">
                    ${album.getPerformer().getName()}
                </a>
              </td>
            </tr>
          </c:forEach>
          </tbody>
        </table>
      </div>
      <div class="col-md-4 text-center">
        <h1>Performers</h1>
        <table class="table box-shadow">
          <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
          </tr>
          </thead>
          <tbody>
          <c:forEach items="${performers}" var="performer" end="7" varStatus="i">
            <tr>
              <td>${i.count}</td>
              <td>
                <c:url value="/track/list.html?performerId=${performer.getId()}&page=1"
                       var="trackByPerformerListUrl"/>
                <a href="${trackByPerformerListUrl}">
                    ${performer.getName()}
                </a>
              </td>
            </tr>
          </c:forEach>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</u:html>
