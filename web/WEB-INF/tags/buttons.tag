<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@attribute name="type" required="true" rtexprvalue="true" type="java.lang.String" %>
<%@attribute name="entity" required="true" rtexprvalue="true" type="by.epam.epamMusic.entity.Entity" %>
<%@attribute name="user" required="true" rtexprvalue="true" type="by.epam.epamMusic.entity.User" %>
<c:choose>
  <c:when test="${user.role.identity eq 0}">
    <c:url value="/${type}/edit.html" var="editUrl"/>
    <c:url value="/${type}/delete.html" var="deleteUrl"/>
    <form style="display: none" id="form-${entity.id}" action="${editUrl}">
      <input type="hidden" name="id" value="${entity.id}">
    </form>
    <form style="display: none" id="form-delete-${entity.id}" action="${deleteUrl}">
      <input type="hidden" name="id" value="${entity.id}">
    </form>
    <button class="btn" type="submit" onclick="sendRequest('form-${entity.id}')">
      Edit
    </button>
    <button class="btn" type="submit" onclick="sendRequest('form-delete-${entity.id}')">
      Delete
    </button>
  </c:when>
  <c:otherwise>
    <c:url value="/user/${type}AddToCart.html" var="addToCartUrl"/>
    <c:choose>
      <c:when test="${user.cart.isInCart(entity) or user.cart.freeTracks.contains(entity)}">
        <strong>Already in cart</strong>
      </c:when>
      <c:when test="${ownedTracks.contains(entity)}">
        <strong style="color: darkgreen">Already owned</strong>
      </c:when>
      <c:otherwise>
        <form style="display: none" id="form-add-to-cart-${entity.id}" action="${addToCartUrl}">
          <input type="hidden" name="id" value="${entity.id}">
        </form>
        <button class="btn" type="submit" onclick="sendRequest('form-add-to-cart-${entity.id}')">
          Add to cart
        </button>

      </c:otherwise>
    </c:choose>
  </c:otherwise>
</c:choose>
