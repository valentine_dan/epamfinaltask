<%@tag language="java" pageEncoding="UTF-8" %>
<%@attribute name="user" required="true" rtexprvalue="true" type="by.epam.epamMusic.entity.User" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
  <c:url value="/user/index.html" var="indexUrl"/>
  <c:url value="/signOut.html" var="signInUrl"/>
  <c:url value="/user/profile.html" var="profileUrl"/>
  <c:url value="/user/cart.html" var="cartUrl"/>
  <c:url value="/track/list.html?page=1" var="trackListUrl"/>
  <c:url value="/album/list.html?page=1" var="albumListUrl"/>
  <c:url value="/performer/list.html?page=1" var="performerListUrl"/>
  <img class="mr-2" src="<c:url value="/img/logo.jpg"/>" style="width:40px;" alt="Logo">
  <a class="my-0 link" href="${indexUrl}"><i class="fas fa-home"></i> Home</a>
  <nav class="ml-4 my-2 my-md-0 mr-md-3 mr-md-auto">
    <c:if test="${user.role.identity eq 0}">
      <c:url value="/user/list.html" var="userListUrl"/>
      <a class="p-2 link" href="${userListUrl}"><i class="fas fa-users"></i> Users</a>
    </c:if>
    <a class="p-2 link" href="${trackListUrl}"><i class="fas fa-music"></i> Tracks</a>
    <a class="p-2 link" href="${albumListUrl}"><i class="fas fa-compact-disc"></i> Albums</a>
    <a class="p-2 link" href="${performerListUrl}"><i class="fas fa-microphone"></i> Performers</a>
  </nav>
  <nav class="my-2 my-md-0 mr-md-3">
    <a class="p-2"><i class="fas fa-user"></i> ${user.getLogin()}</a>
    <a class="p-2"><i class="fas fa-money-bill-alt"></i> ${user.getBalance()}</a>
    <a class="p-2 link" href="${profileUrl}"><i class="fas fa-user-alt"></i> Profile</a>
    <a class="p-2 link" href="${cartUrl}"><i class="fas fa-shopping-cart"></i> Your cart</a>
  </nav>
  <a class="btn btn-outline-primary" href="${signInUrl}">Sign out</a>
</div>