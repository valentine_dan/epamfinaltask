<%@tag language="java" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@attribute name="type" required="true" rtexprvalue="true" type="java.lang.String" %>
<%@attribute name="url" required="false" rtexprvalue="true" type="java.lang.String" %>
<%@attribute name="msg" required="false" rtexprvalue="true" type="java.lang.String" %>
<%@attribute name="title" required="true" rtexprvalue="true" type="java.lang.String" %>


<div class="modal fade" id="${type}-modal">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">${title}</h4>
      </div>
        <c:choose>
          <c:when test="${not empty url}">
            <c:url value="${url}" var="url"/>
            <form id="${type}-form" name="${type}" method="post" action="${url}">
              <div class="container form-group">
                <jsp:doBody/>
              </div>
            </form>
          </c:when>
          <c:when test="${not empty msg}">
            <p>${msg}</p>
            <div class="modal-footer">
              <button type="button" class="btn" data-dismiss="modal">Close</button>
            </div>
            <script>
                $(document).ready(function () {
                    $("#${type}-modal").modal("show");
                });
            </script>
          </c:when>
        </c:choose>
    </div>
  </div>
</div>