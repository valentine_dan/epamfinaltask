<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
  <title>Sign up</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  <link href="css/form.css" rel="stylesheet">

</head>
<body>
<div class="container">
  <div class="text-center">
    <h3></h3>
    <img class="mb-4" src="img/logo.jpg" alt="" style="width: 70px">
    <h2>Signing up</h2>
  </div>
</div>
<div class="form-content container">
  <c:url value="/signUp.html" var="signUpUrl"/>
  <form class="needs-validation" method="post" action="${signUpUrl}">
    <div class="row">
      <div class="col-md-6 mb-3">
        <label for="firstName">Name</label>
        <input type="text" class="form-control" name="name" id="firstName" placeholder="" value="" required>
        <div class="invalid-feedback">
          Valid name is required.
        </div>
      </div>
      <div class="col-md-6 mb-3">
        <label for="lastName">Surname</label>
        <input type="text" class="form-control" name="surname" id="lastName" placeholder="" value="" required>
        <div class="invalid-feedback">
          Valid surname is required.
        </div>
      </div>
    </div>

    <div class="mb-3">
      <label for="login">Login</label>
      <input type="text" class="form-control" name="login" id="login" pattern="[A-Za-z]([A-Za-z0-9._]{1,17})[A-Za-z0-9]"
             placeholder="Login" required>
      <div class="invalid-feedback" style="width: 100%;">
        Your login is required.
      </div>
    </div>

    <div class="mb-3">
      <label for="email">Email</label>
      <input type="email" class="form-control" name="email" id="email" placeholder="you@example.com" required>
      <div class="invalid-feedback">
        Please enter a valid email address.
      </div>
    </div>
    <div class="mb-3">
      <label for="pswd">Password</label>
      <input class="form-control" type="password" name="password" id="pswd"
             pattern="[A-Za-z0-9]([A-Za-z0-9._]{5,23})[A-Za-z0-9]"
             placeholder="********"
             required>
      <label for="repPswd">Repeat password</label>
      <input class="form-control" type="password" name="repeatPassword" id="repPswd"
             pattern="[A-Za-z0-9]([A-Za-z0-9._]{5,23})[A-Za-z0-9]"
             placeholder="********"
             required>
    </div>
    <button class="btn btn-lg btn-block" type="submit">Sign up</button>
  </form>
</div>
</body>
</html>
