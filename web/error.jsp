<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="u" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<u:html user="${user}" title="Error">
  <div class = "text-center">
    <h2>Error!</h2>
    <p>${message}</p>
  </div>
</u:html>
