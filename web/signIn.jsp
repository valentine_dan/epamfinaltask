<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
  <title>Sign in</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  <link href="css/form.css" rel="stylesheet">
</head>
<body>
<div class="container form-content text-center">
  <c:url value="/signIn.html" var="signInUrl"/>
  <form class="form-signin" method="post" action="${signInUrl}">
    <img class="mb-4" src="img/logo.jpg" alt="" style="width: 70px">
    <h1 class="h3 mb-3 font-weight-normal">Please, sign in</h1>
    <c:if test="${not empty message}">
      <p style="color: red">${message}</p>
    </c:if>
    <input type="text" name="login" class="form-control" placeholder="Login" required autofocus>
    <input type="password" name="password" class="form-control" placeholder="Password" required>
    <button class="btn btn-lg btn-block" type="submit">Sign in</button>
    <p>New on <b style="color: red">epam music</b>? <a class="create-account" href="signUp.jsp">Create account.</a></p>
  </form>
</div>
</body>
</html>
