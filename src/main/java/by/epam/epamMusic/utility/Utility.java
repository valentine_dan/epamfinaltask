package by.epam.epamMusic.utility;

import by.epam.epamMusic.entity.Track;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class Utility {
    public static Set<Track> intersection(Collection<Track> collection1, Collection<Track> collection2) {
        Set<Track> resultSet = new HashSet<>();
        for (Track track1 : collection1) {
            boolean isIn2Col = false;
            for (Track track2 :
                    collection2) {
                if (track1.getId().equals(track2.getId())) {
                    isIn2Col = true;
                }
            }
            if (!isIn2Col) {
                resultSet.add(track1);
            }
        }
        return resultSet;
    }
}
