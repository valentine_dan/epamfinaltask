package by.epam.epamMusic.validator;

public class FormValidator {
    private static final String LOGIN_REGEXP = "[A-Za-z]([A-Za-z0-9._]{1,17})[A-Za-z0-9]";
    private static final String PASSWORD_REGEXP = "[A-Za-z0-9]([A-Za-z0-9._]{5,23})[A-Za-z0-9]";
    private static final String EMAIL_REGEXP = "[A-Za-z0-9_.]+@[A-Za-z]+\\.[A-Za-z]+";

    public static boolean isLoginValid(String login){
        return login.matches(LOGIN_REGEXP);
    }

    public static boolean isPasswordValid(String password){
        return password.matches(PASSWORD_REGEXP);
    }

    public static boolean isEmailValid(String email){
        return email.matches(EMAIL_REGEXP);
    }
}
