package by.epam.epamMusic.validator;

import java.math.BigDecimal;

public class TrackValidator {
    public static boolean validateName(String trackName) {
        return trackName.length() > 0;
    }

    public static boolean validatePrice(BigDecimal price) {
        return price.compareTo(new BigDecimal(0)) >= 0;
    }
}
