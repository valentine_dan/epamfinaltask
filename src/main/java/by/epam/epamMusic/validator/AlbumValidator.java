package by.epam.epamMusic.validator;

public class AlbumValidator {
    public static boolean validateName(String trackName) {
        return trackName.length() > 0;
    }

    public static boolean validateYear(Integer year) {
        return String.valueOf(year).matches("[1-9]([0-9]{3})");
    }
}
