package by.epam.epamMusic.dao;

import by.epam.epamMusic.entity.Playlist;

public interface PlaylistDao extends Dao<Playlist> {
}
