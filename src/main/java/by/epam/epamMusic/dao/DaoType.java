package by.epam.epamMusic.dao;

public enum DaoType {
    ALBUM,
    BANK_ACCOUNT,
    BONUS,
    PERFORMER,
    TRACK,
    USER;
}
