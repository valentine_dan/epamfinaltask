package by.epam.epamMusic.dao;

import by.epam.epamMusic.entity.Track;
import by.epam.epamMusic.exception.IntrusiveException;

import java.util.List;

public interface TrackDao extends Dao<Track> {
    List<Track> findByAlbumId(Integer id)
            throws IntrusiveException;

    List<Track> findByPerformerId(Integer performerId)
            throws IntrusiveException;

    boolean isOwned(Integer id)
            throws IntrusiveException;

    List<Track> findByUserId(Integer userId)
            throws IntrusiveException;
}
