package by.epam.epamMusic.dao;

import by.epam.epamMusic.exception.IntrusiveException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.ReentrantLock;

public class ConnectionPool {
    private static Logger LOG = LogManager.getLogger(ConnectionPool.class);
    private static ConnectionPool instance;
    private BlockingQueue<ProxyConnection> freeConnections = new LinkedBlockingQueue<>();
    private BlockingQueue<ProxyConnection> busyConnections = new LinkedBlockingQueue<>();
    private static ReentrantLock lock = new ReentrantLock();
    private static final Integer INITIAL_POOL_SIZE = 20;
    private final static ResourceBundle bundle = ResourceBundle.getBundle("properties.database");

    public void initializePool() throws IntrusiveException {
        Properties properties = new Properties();
        properties.setProperty("user", bundle.getString("user"));
        properties.setProperty("password", bundle.getString("password"));
        properties.setProperty("encoding", bundle.getString("encoding"));
        try {
            for (int i = 0; i < INITIAL_POOL_SIZE; i++) {
                Connection connection = DriverManager.getConnection(bundle.getString("url"), properties);
                ProxyConnection proxyConnection = new ProxyConnection(connection);
                freeConnections.offer(proxyConnection);
            }
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        }
    }

    private ConnectionPool(){

    }

    public static ConnectionPool getInstance() {
        lock.lock();
        try {
            if (instance == null) {
                instance = new ConnectionPool();
                return instance;
            } else {
                return instance;
            }
        } finally {
            lock.unlock();
        }
    }

    public ProxyConnection obtainConnection() throws IntrusiveException {
        ProxyConnection proxyConnection = null;
        lock.lock();
        try {
            if (!freeConnections.isEmpty()) {
                proxyConnection = freeConnections.take();
                busyConnections.add(proxyConnection);
            }
            return proxyConnection;
        } catch (InterruptedException e) {
            throw new IntrusiveException(e);
        }
        finally {
            lock.unlock();
        }
    }

    public void returnConnection(ProxyConnection returnedConnection) {
        lock.lock();
        try {
            busyConnections.remove(returnedConnection);
            freeConnections.offer(returnedConnection);
        }finally {
            lock.unlock();
        }


    }

    public void free() {
        closeConnections(freeConnections);
        closeConnections(busyConnections);
    }

    private void closeConnections(BlockingQueue<ProxyConnection> connections) {
        ProxyConnection connection = null;
        try {
            while ((connection = connections.poll()) != null) {
                connection.getConnection().close();
            }

        } catch (SQLException e) {
            LOG.error("Unable to close connection", e);

        }
    }

    public BlockingQueue<ProxyConnection> getFreeConnections() {
        return freeConnections;
    }
}
