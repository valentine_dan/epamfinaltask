package by.epam.epamMusic.dao;

import by.epam.epamMusic.entity.Performer;

public interface PerformerDao extends Dao<Performer> {
}
