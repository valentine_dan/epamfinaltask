package by.epam.epamMusic.dao.mysql;

import by.epam.epamMusic.dao.Dao;
import by.epam.epamMusic.dao.UserDao;

import java.sql.Connection;

public abstract class AbstractDaoImpl{
    protected Connection connection;

    public void setConnection(Connection connection) {
        this.connection = connection;
    }
}
