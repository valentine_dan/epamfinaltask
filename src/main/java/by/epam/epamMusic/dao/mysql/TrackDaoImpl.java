package by.epam.epamMusic.dao.mysql;

import by.epam.epamMusic.dao.TrackDao;
import by.epam.epamMusic.entity.Album;
import by.epam.epamMusic.entity.Performer;
import by.epam.epamMusic.entity.Track;
import by.epam.epamMusic.exception.IntrusiveException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.taglibs.standard.extra.spath.Step;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TrackDaoImpl extends AbstractDaoImpl implements TrackDao {
    private static Logger LOG = LogManager.getLogger(TrackDaoImpl.class);

    @Override
    public void create(Track track)
            throws IntrusiveException {
        String sql = "insert into " +
                " track (name, price, album_id)" +
                " VALUES (?,?,?)";
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setString(1, track.getName());
            statement.setBigDecimal(2, track.getPrice());
            statement.setInt(3, track.getAlbum().getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        } finally {
            try {
                closeResources(statement, null);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }
    }

    @Override
    public Track read(Integer id)
            throws IntrusiveException {
        String sql = "select" +
                " t.id as trackId, t.name, t.price," +
                " a.id as albumId, a.name as albumName," +
                " p.id as performerId, p.name as performerName" +
                " from track t " +
                " join album a on t.album_id = a.id" +
                " join performer p on a.performer_id = p.id " +
                " where t.id=?";
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Track track = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                track = new Track();
                track.setId(resultSet.getInt("trackId"));
                track.setName(resultSet.getString("name"));
                track.setPrice(resultSet.getBigDecimal("price"));
                Performer performer = new Performer();
                performer.setId(resultSet.getInt("performerId"));
                performer.setName(resultSet.getString("performerName"));
                Album album = new Album();
                album.setId(resultSet.getInt("albumId"));
                album.setName(resultSet.getString("albumName"));
                album.setPerformer(performer);
                track.setAlbum(album);
            }
            return track;
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        } finally {
            try {
                closeResources(statement, resultSet);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }
    }

    @Override
    public List<Track> read()
            throws IntrusiveException {
        String sql = "select" +
                " t.id as trackId, t.name, t.price," +
                " a.id as albumId, a.name as albumName," +
                " p.id as performerId, p.name as performerName" +
                " from track t " +
                " join album a on t.album_id = a.id" +
                " join performer p on a.performer_id = p.id";
        List<Track> tracks = new ArrayList<>();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Track track = new Track();
                track.setId(resultSet.getInt("trackId"));
                track.setName(resultSet.getString("name"));
                track.setPrice(resultSet.getBigDecimal("price"));
                Performer performer = new Performer();
                performer.setId(resultSet.getInt("performerId"));
                performer.setName(resultSet.getString("performerName"));
                Album album = new Album();
                album.setId(resultSet.getInt("albumId"));
                album.setName(resultSet.getString("albumName"));
                album.setPerformer(performer);
                track.setAlbum(album);
                tracks.add(track);
            }
            return tracks;
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        } finally {
            try {
                closeResources(statement, resultSet);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }
    }

    @Override
    public void update(Track track)
            throws IntrusiveException {
        String sql = "update" +
                " track t" +
                " join album a on t.album_id = a.id" +
                " set" +
                " t.name=?, t.price=?, t.album_id=?" +
                " where t.id=?";
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setString(1, track.getName());
            statement.setBigDecimal(2, track.getPrice());
            statement.setInt(3, track.getAlbum().getId());
            statement.setInt(4, track.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        } finally {
            try {
                closeResources(statement, null);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }
    }

    @Override
    public void delete(Integer id)
            throws IntrusiveException {
        String sql = "delete from track where id=?";
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        } finally {
            try {
                closeResources(statement, null);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }
    }

    @Override
    public List<Track> findByUserId(Integer userId)
            throws IntrusiveException {
        String sql = "select" +
                " t.id as trackId, t.name as trackName," +
                " price, " +
                " a.id as albumId, a.name as albumName," +
                " p.id as performerId, p.name as performerName" +
                " from track t" +
                " join album a on t.album_id = a.id" +
                " join performer p on a.performer_id = p.id" +
                " join usertrack u on t.id = u.track_id" +
                " where u.user_id=?";
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Track> tracks = new ArrayList<>();
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1, userId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Track track = new Track();
                track.setId(resultSet.getInt("trackId"));
                track.setName(resultSet.getString("trackName"));
                track.setPrice(resultSet.getBigDecimal("price"));
                Performer performer = new Performer();
                performer.setId(resultSet.getInt("performerId"));
                performer.setName(resultSet.getString("performerName"));
                Album album = new Album();
                album.setId(resultSet.getInt("albumId"));
                album.setName(resultSet.getString("albumName"));
                album.setPerformer(performer);
                track.setAlbum(album);
                tracks.add(track);
            }
            return tracks;
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        } finally {
            try {
                closeResources(statement, resultSet);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }
    }

    @Override
    public List<Track> findByAlbumId(Integer id)
            throws IntrusiveException {
        String sql = "select" +
                " t.id as trackId, t.name as trackName," +
                " t.price," +
                " a.id as albumId, a.name as albumName," +
                " a.year, " +
                " p.id as performerId, p.name as performerName" +
                " from track t " +
                " join album a on t.album_id = a.id" +
                " join performer p on a.performer_id = p.id" +
                " where t.album_id=?" +
                "";
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Track> tracks = new ArrayList<>();
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Track track = new Track();
                Album album = new Album();
                Performer performer = new Performer();
                performer.setId(resultSet.getInt("performerId"));
                performer.setName(resultSet.getString("performerName"));
                album.setPerformer(performer);
                album.setId(resultSet.getInt("albumId"));
                album.setYear(resultSet.getInt("year"));
                album.setName(resultSet.getString("albumName"));
                track.setId(resultSet.getInt("trackId"));
                track.setPrice(resultSet.getBigDecimal("price"));
                track.setName(resultSet.getString("trackName"));
                track.setAlbum(album);
                tracks.add(track);
            }
            return tracks;
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        } finally {
            try {
                closeResources(statement, resultSet);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }
    }

    @Override
    public boolean isOwned(Integer id)
            throws IntrusiveException {
        String sql = "select user_id from usertrack where track_id=?";
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            return resultSet.next();
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        } finally {
            try {
                closeResources(statement, resultSet);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }
    }

    @Override
    public List<Track> findByPerformerId(Integer performerId)
            throws IntrusiveException {
        String sql = "select " +
                "t.id as trackId, t.name as trackName," +
                "price, " +
                "a.id as albumId, a.name as albumName," +
                "p.id as performerId, p.name as performerName " +
                "from track t " +
                "join album a on t.album_id = a.id " +
                "join performer p on a.performer_id = p.id " +
                "where p.id=?";
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Track> tracks = new ArrayList<>();
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1, performerId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Track track = new Track();
                track.setId(resultSet.getInt("trackId"));
                track.setName(resultSet.getString("trackName"));
                track.setPrice(resultSet.getBigDecimal("price"));
                Performer performer = new Performer();
                performer.setId(resultSet.getInt("performerId"));
                performer.setName(resultSet.getString("performerName"));
                Album album = new Album();
                album.setId(resultSet.getInt("albumId"));
                album.setName(resultSet.getString("albumName"));
                album.setPerformer(performer);
                track.setAlbum(album);
                tracks.add(track);
            }
            return tracks;
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        } finally {
            try {
                closeResources(statement, resultSet);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }
    }
}
