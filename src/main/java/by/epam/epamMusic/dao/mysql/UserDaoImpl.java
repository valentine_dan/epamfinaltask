package by.epam.epamMusic.dao.mysql;

import by.epam.epamMusic.dao.UserDao;
import by.epam.epamMusic.entity.BankAccount;
import by.epam.epamMusic.entity.Entity;
import by.epam.epamMusic.entity.Role;
import by.epam.epamMusic.entity.User;
import by.epam.epamMusic.exception.IntrusiveException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class UserDaoImpl extends AbstractDaoImpl implements UserDao {
    private static Logger LOG = LogManager.getLogger(UserDaoImpl.class);

    @Override
    public void create(User user)
            throws IntrusiveException {
        String sql = "INSERT INTO user " +
                "(`login`, `password`, `role`," +
                " `name`, `surname`, `email`)" +
                " VALUES (?,md5(?),?,?,?,?)";
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPassword());
            statement.setInt(3, user.getRole().getIdentity());
            statement.setString(4, user.getName());
            statement.setString(5, user.getSurname());
            statement.setString(6, user.getEmail());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        } finally {
            try {
                closeResources(statement, null);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }
    }

    @Override
    public void addPaymentMethod(User user)
            throws IntrusiveException {
        String sql = "update user set bankAccount_id=? where id=?";
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1, user.getBankAccount().getId());
            statement.setInt(2, user.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        } finally {
            try {
                closeResources(statement, null);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }
    }

    @Override
    public void updateBalance(User user)
            throws IntrusiveException {
        String sql = "update user set moneyAmount=? where id=?";
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setBigDecimal(1, user.getBalance());
            statement.setInt(2, user.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        } finally {
            try {
                closeResources(statement, null);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }
    }

    @Override
    public User read(Integer id)
            throws IntrusiveException {
        String sql = "SELECT u.id as userId," +
                " `login`, `password`," +
                " `role`, `name`," +
                " `surname`, `email`," +
                " `bankAccount_id`, `moneyAmount`," +
                " b.number, b.availableMoney " +
                "FROM user u " +
                "JOIN bankaccount b on u.bankAccount_id = b.id" +
                " WHERE u.id=?";
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        User user = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                user = new User();
                user.setId(resultSet.getInt("userId"));
                user.setLogin(resultSet.getString("login"));
                user.setPassword(resultSet.getString("password"));
                user.setRole(Role.getById(resultSet.getInt("role")));
                user.setName(resultSet.getString("name"));
                user.setSurname(resultSet.getString("surname"));
                user.setEmail(resultSet.getString("email"));
                user.setBalance(resultSet.getBigDecimal("moneyAmount"));
            }
            return user;
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        } finally {
            try {
                closeResources(statement, resultSet);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }
    }

    @Override
    public void update(User user)
            throws IntrusiveException {
        String sql = "update user set login=?, password=md5(?), role=?," +
                " name=?, surname=?, email=?" +
                " where id=?";
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPassword());
            statement.setInt(3, user.getRole().getIdentity());
            statement.setString(4, user.getName());
            statement.setString(5, user.getSurname());
            statement.setString(6, user.getEmail());
            statement.setInt(7, user.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        } finally {
            try {
                closeResources(statement, null);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }
    }

    @Override
    public void delete(Integer id) throws IntrusiveException {
        String sql = "delete from user where id=?";
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        } finally {
            try {
                closeResources(statement, null);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }
    }

    @Override
    public List<User> read() throws IntrusiveException {
        String sql = "SELECT `id`, `login`, `password`," +
                " `role`, `name`, `surname`," +
                " `email`, `moneyAmount`" +
                " FROM `User`";
        List<User> users = new ArrayList<>();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                User user = new User();
                user.setId(resultSet.getInt("id"));
                user.setLogin(resultSet.getString("login"));
                user.setPassword(resultSet.getString("password"));
                user.setRole(Role.getById(resultSet.getInt("role")));
                user.setName(resultSet.getString("name"));
                user.setSurname(resultSet.getString("surname"));
                user.setEmail(resultSet.getString("email"));
                user.setBalance(resultSet.getBigDecimal("moneyAmount"));
                users.add(user);
            }
            return users;
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        } finally {
            try {
                closeResources(statement, resultSet);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }
    }

    @Override
    public User findByLoginAndPassword(String login, String password) throws IntrusiveException {
        String sql = "select" +
                " u.id, login, password," +
                " role, name, surname," +
                " email, bankAccount_id, moneyAmount" +
                " from user u" +
                " where u.login=? AND u.password=md5(?)";
        String sql2 = "select" +
                " b.number," +
                " b.availableMoney," +
                " bankAccount_id" +
                " from user u" +
                " join bankaccount b on u.bankAccount_id = b.id" +
                " where login=? AND password=md5(?)";
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        ResultSet resultSet_ifHasAccount = null;
        User user = null;
        try {
            connection.setAutoCommit(false);
            statement = connection.prepareStatement(sql);
            statement.setString(1, login);
            statement.setString(2, password);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                user = new User();
                user.setId(resultSet.getInt("id"));
                user.setLogin(resultSet.getString("login"));
                user.setPassword(resultSet.getString("password"));
                user.setRole(Role.getById(resultSet.getInt("role")));
                user.setName(resultSet.getString("name"));
                user.setSurname(resultSet.getString("surname"));
                user.setEmail(resultSet.getString("email"));
                user.setBalance(resultSet.getBigDecimal("moneyAmount"));
                resultSet.getInt("bankAccount_id");
                if (!resultSet.wasNull()) {
                    statement = connection.prepareStatement(sql2);
                    statement.setString(1, login);
                    statement.setString(2, password);
                    resultSet_ifHasAccount = statement.executeQuery();
                    if (resultSet_ifHasAccount.next()) {
                        BankAccount bankAccount = new BankAccount();
                        bankAccount.setId(
                                resultSet_ifHasAccount.getInt("bankAccount_id"));
                        bankAccount.setNumber(resultSet_ifHasAccount.getInt("number"));
                        bankAccount.setAvailableMoney(resultSet_ifHasAccount.getBigDecimal("availableMoney"));
                        user.setBankAccount(bankAccount);
                    }
                }
                connection.commit();
                return user;
            }
            return user;
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        } finally {
            try {
                connection.setAutoCommit(true);
                closeResources(statement, resultSet);
                closeResources(statement, resultSet_ifHasAccount);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }

    }

    @Override
    public User findByLogin(String login) throws IntrusiveException {
        String sql = "SELECT" +
                " `id`, `login`, `password`," +
                " `role`, `name`, `surname`," +
                " `email`, `moneyAmount`" +
                " FROM `User`" +
                " WHERE `login`=?";
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        User user = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setString(1, login);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                user = new User();
                user.setId(resultSet.getInt("id"));
                user.setLogin(resultSet.getString("login"));
                user.setPassword(resultSet.getString("password"));
                user.setRole(Role.getById(resultSet.getInt("role")));
                user.setName(resultSet.getString("name"));
                user.setSurname(resultSet.getString("surname"));
                user.setEmail(resultSet.getString("email"));
                user.setBalance(resultSet.getBigDecimal("moneyAmount"));
            }
            return user;
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        } finally {
            try {
                closeResources(statement, resultSet);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }
    }

    @Override
    public void buyTrack(Integer trackId,
                         Integer userId,
                         String paymentDate,
                         Integer paymentNumber) throws IntrusiveException {
        String sql = "call buyTrack(?,?,?,?)";
        CallableStatement statement = null;
        try {
            statement = connection.prepareCall(sql);
            statement.setInt("userId", userId);
            statement.setInt("trackId", trackId);
            statement.setString("paymentDate", paymentDate);
            statement.setInt("paymentNumb", paymentNumber);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        } finally {
            try {
                closeResources(statement, null);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }
    }

}
