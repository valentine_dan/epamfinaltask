package by.epam.epamMusic.dao.mysql;

import by.epam.epamMusic.dao.*;
import by.epam.epamMusic.entity.Entity;
import by.epam.epamMusic.exception.IntrusiveException;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class DaoFactoryImpl implements DaoFactory {

    private static Map<DaoType, AbstractDaoImpl> dataAccessObjects = new HashMap<>();

    private ProxyConnection connection;

    static {
        dataAccessObjects.put(DaoType.ALBUM, new AlbumDaoImpl());
        dataAccessObjects.put(DaoType.TRACK, new TrackDaoImpl());
        dataAccessObjects.put(DaoType.PERFORMER, new PerformerDaoImpl());
        dataAccessObjects.put(DaoType.USER, new UserDaoImpl());
        dataAccessObjects.put(DaoType.BONUS, new BonusDaoImpl());
        dataAccessObjects.put(DaoType.BANK_ACCOUNT, new BankAccountDaoImpl());
    }

    public DaoFactoryImpl() throws IntrusiveException {
        connection = ConnectionPool.getInstance().obtainConnection();
    }

    @Override
    public <T extends Dao<? extends Entity>> T createDataAccessObject(DaoType type) throws IntrusiveException {
        AbstractDaoImpl abstractDao = dataAccessObjects.get(type);
        abstractDao.setConnection(connection);
        @SuppressWarnings("unchecked")
        T dao = (T) abstractDao;
        return dao;
    }

    @Override
    public void close() {
        connection.close();
    }
}
