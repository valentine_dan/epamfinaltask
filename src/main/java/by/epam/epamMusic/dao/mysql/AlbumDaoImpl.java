package by.epam.epamMusic.dao.mysql;

import by.epam.epamMusic.dao.AlbumDao;
import by.epam.epamMusic.entity.Album;
import by.epam.epamMusic.entity.Performer;
import by.epam.epamMusic.exception.IntrusiveException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AlbumDaoImpl extends AbstractDaoImpl implements AlbumDao {
    private static Logger LOG = LogManager.getLogger(AlbumDaoImpl.class);

    @Override
    public void create(Album album)
            throws IntrusiveException {
        String sql = "insert into " +
                " album (name, year, performer_id)" +
                " VALUE (?,?,?)";
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setString(1, album.getName());
            statement.setInt(2, album.getYear());
            statement.setInt(3, album.getPerformer().getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        }finally {
            try {
                closeResources(statement, null);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }
    }

    @Override
    public Album read(Integer id)
            throws IntrusiveException {
        String sql = "select" +
                " a.name, year, performer_id," +
                " p.name as performerName," +
                " cover " +
                " from album a " +
                " join performer p on a.performer_id = p.id" +
                " where a.id=?";
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Album album = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            if(resultSet.next()){
                album = new Album();
                album.setId(id);
                album.setName(resultSet.getString("name"));
                album.setYear(resultSet.getInt("year"));
                Performer performer = new Performer();
                performer.setId(resultSet.getInt("performer_id"));
                performer.setName(resultSet.getString("performerName"));
                album.setPerformer(performer);
                album.setCover(new File(resultSet.getString("cover")));
            }
            return album;
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        }finally {
            try {
                closeResources(statement, resultSet);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }
    }

    @Override
    public List<Album> read()
            throws IntrusiveException {
        String sql = "select" +
                " a.id, a.name, year," +
                " performer_id, p.name as performerName," +
                " cover " +
                " from album a " +
                " join performer p on a.performer_id = p.id";

        List<Album> albums = new ArrayList<>();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Album album = new Album();
                album.setId(resultSet.getInt("id"));
                album.setName(resultSet.getString("name"));
                album.setYear(resultSet.getInt("year"));
                Performer performer = new Performer();
                performer.setId(resultSet.getInt("performer_id"));
                performer.setName(resultSet.getString("performerName"));
                album.setPerformer(performer);
                album.setCover(new File(resultSet.getString("cover")));
                albums.add(album);
            }
            return albums;
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        }finally {
            try {
                closeResources(statement, resultSet);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }
    }

    @Override
    public void update(Album album)
            throws IntrusiveException {
        String sql = "update album set name=?, year=?, performer_id=? where id=?";
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setString(1, album.getName());
            statement.setInt(2, album.getYear());
            statement.setInt(3, album.getPerformer().getId());
            statement.setInt(4, album.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        }finally {
            try {
                closeResources(statement, null);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }
    }

    @Override
    public void delete(Integer id)
            throws IntrusiveException {
        String sql="delete from album where id=?";
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        }finally {
            try {
                closeResources(statement, null);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }
    }

    @Override
    public List<Album> findByPerformerId(Integer performerId)
            throws IntrusiveException {
        String sql = "select" +
                " a.id, a.name, a.year," +
                " a.performer_id," +
                " p.name as performerName " +
                "from album a " +
                "join performer p on a.performer_id = p.id " +
                "where p.id=?";
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Album> albums = new ArrayList<>();
        try{
            statement = connection.prepareStatement(sql);
            statement.setInt(1, performerId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Performer performer = new Performer();
                performer.setId(resultSet.getInt("performer_id"));
                performer.setName(resultSet.getString("performerName"));
                Album album = new Album();
                album.setId(resultSet.getInt("id"));
                album.setName(resultSet.getString("name"));
                album.setYear(resultSet.getInt("year"));
                album.setPerformer(performer);
                albums.add(album);
            }
            return albums;

        } catch (SQLException e) {
            throw new IntrusiveException(e);
        }finally {
            try {
                closeResources(statement, resultSet);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }
    }
}
