package by.epam.epamMusic.dao.mysql;

import by.epam.epamMusic.dao.BonusDao;
import by.epam.epamMusic.entity.Bonus;
import by.epam.epamMusic.exception.IntrusiveException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BonusDaoImpl extends AbstractDaoImpl implements BonusDao {
    private static Logger LOG = LogManager.getLogger(BonusDaoImpl.class);

    @Override
    public void create(Bonus entity)
            throws IntrusiveException {
        String sql = "insert into bonus (value, type) VALUES (?,?)";
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setDouble(1, entity.getDiscountValue());
            String a = entity.getBonusType().toString();
            statement.setString(2, entity.getBonusType().toString());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        } finally {
            try {
                closeResources(statement, null);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }
    }

    @Override
    public Bonus read(Integer id)
            throws IntrusiveException {
        String sql = "select id, `value`, type from bonus where id=?";
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            Bonus bonus = null;
            if (resultSet.next()) {
                bonus = new Bonus();
                bonus.setId(resultSet.getInt("id"));
                bonus.setDiscountValue(resultSet.getDouble("value"));
                bonus.setBonusType(Bonus.Type.valueOf(resultSet.getString("type")));
            }
            return bonus;
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        } finally {
            try {
                closeResources(statement, resultSet);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }
    }

    @Override
    public List<Bonus> read()
            throws IntrusiveException {
        String sql = "select id, `value`, type from bonus";
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Bonus> bonuses = new ArrayList<>();
        try {
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Bonus bonus = new Bonus();
                bonus.setId(resultSet.getInt("id"));
                bonus.setDiscountValue(resultSet.getDouble("value"));
                bonus.setBonusType(Bonus.Type.valueOf(resultSet.getString("type")));
                bonuses.add(bonus);
            }
            return bonuses;
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        } finally {
            try {
                closeResources(statement, resultSet);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }
    }

    @Override
    public void update(Bonus entity)
            throws IntrusiveException {
        String sql = "update bonus set value=?, type=? where id=?";
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setDouble(1, entity.getDiscountValue());
            statement.setString(2, entity.getBonusType().toString());
            statement.setInt(3, entity.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        } finally {
            try {
                closeResources(statement, null);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }
    }

    @Override
    public void delete(Integer id)
            throws IntrusiveException {
        String sql = "delete from bonus where id=?";
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        } finally {
            try {
                closeResources(statement, null);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }
    }

    @Override
    public List<Bonus> readByUserId(Integer userId)
            throws IntrusiveException {
        String sql = "select b.id, b.value, b.type" +
                " from userbonus u" +
                " join bonus b on u.bonus_id = b.id" +
                " where u.user_id=?";
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Bonus> bonuses = new ArrayList<>();
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1, userId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Bonus bonus = new Bonus();
                bonus.setId(resultSet.getInt("id"));
                bonus.setDiscountValue(resultSet.getDouble("value"));
                bonus.setBonusType(Bonus.Type.valueOf(resultSet.getString("type")));
                bonuses.add(bonus);
            }
            return bonuses;
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        } finally {
            try {
                closeResources(statement, resultSet);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }
    }

    @Override
    public List<Bonus> readByType(Bonus.Type type)
            throws IntrusiveException {
        String sql = "select id, `value`, type from bonus where type=?";
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Bonus> bonuses = new ArrayList<>();
        try {
            statement = connection.prepareStatement(sql);
            String a = type.toString();
            statement.setString(1, type.toString());
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Bonus bonus = new Bonus();
                bonus.setId(resultSet.getInt("id"));
                bonus.setDiscountValue(resultSet.getDouble("value"));
                bonus.setBonusType(Bonus.Type.valueOf(resultSet.getString("type")));
                bonuses.add(bonus);
            }
            return bonuses;
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        } finally {
            try {
                closeResources(statement, resultSet);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }
    }

    public Bonus readByDiscountValue(Double value)
            throws IntrusiveException {
        String sql = "select id, `value`, type from bonus where value=?";
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setDouble(1, value);
            resultSet = statement.executeQuery();
            Bonus bonus = null;
            if (resultSet.next()) {
                bonus = new Bonus();
                bonus.setId(resultSet.getInt("id"));
                bonus.setDiscountValue(resultSet.getDouble("value"));
                bonus.setBonusType(Bonus.Type.valueOf(resultSet.getString("type")));
            }
            return bonus;
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        } finally {
            try {
                closeResources(statement, resultSet);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }
    }

    @Override
    public void addBonusToUser(Bonus bonus, Integer userId)
            throws IntrusiveException {
        String sql = "insert into userbonus (bonus_id, user_id) VALUES (?,?)";
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1, bonus.getId());
            statement.setInt(2, userId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        } finally {
            try {
                closeResources(statement, null);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }
    }

    @Override
    public void writeOff(Integer bonusId,
                         Integer userId)
            throws IntrusiveException {
        String sql = "delete from userbonus" +
                " where user_id=? and bonus_id=?";
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1, userId);
            statement.setInt(2, bonusId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        } finally {
            try {
                closeResources(statement, null);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }

    }
}
