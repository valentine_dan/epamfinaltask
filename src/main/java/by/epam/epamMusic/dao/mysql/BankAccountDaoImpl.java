package by.epam.epamMusic.dao.mysql;

import by.epam.epamMusic.dao.BankAccountDao;
import by.epam.epamMusic.entity.BankAccount;
import by.epam.epamMusic.exception.IntrusiveException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BankAccountDaoImpl extends AbstractDaoImpl implements BankAccountDao {
    private static Logger LOG = LogManager.getLogger(BankAccountDaoImpl.class);

    @Override
    public void create(BankAccount entity)
            throws IntrusiveException {
        String sql = "insert into bankaccount (number, availableMoney) VALUES (?,?)";
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1, entity.getNumber());
            statement.setBigDecimal(2, entity.getAvailableMoney());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        } finally {
            try {
                closeResources(statement, null);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }
    }

    @Override
    public BankAccount read(Integer id)
            throws IntrusiveException {
        String sql = "select number, availableMoney from bankaccount where id=?";
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        BankAccount bankAccount = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                bankAccount = new BankAccount();
                bankAccount.setId(id);
                bankAccount.setNumber(resultSet.getInt("number"));
                bankAccount.setAvailableMoney(resultSet.getBigDecimal("availableMoney"));
            }
            return bankAccount;
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        } finally {
            try {
                closeResources(statement, resultSet);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }
    }

    @Override
    public List<BankAccount> read()
            throws IntrusiveException {
        String sql = "select id, number, availableMoney from bankaccount";
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<BankAccount> bankAccounts = new ArrayList<>();
        try {
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                BankAccount bankAccount = new BankAccount();
                bankAccount.setId(resultSet.getInt("id"));
                bankAccount.setNumber(resultSet.getInt("number"));
                bankAccount.setAvailableMoney(resultSet.getBigDecimal("availableMoney"));
                bankAccounts.add(bankAccount);
            }
            return bankAccounts;
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        } finally {
            try {
                closeResources(statement, resultSet);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }
    }

    @Override
    public void update(BankAccount entity)
            throws IntrusiveException {
        String sql = "update bankaccount set number=?, availableMoney=? where id=?";
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1, entity.getNumber());
            statement.setBigDecimal(2, entity.getAvailableMoney());
            statement.setInt(3, entity.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        } finally {
            try {
                closeResources(statement, null);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }
    }

    @Override
    public void delete(Integer id)
            throws IntrusiveException {
        String sql = "delete from bankaccount where id=?";
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        } finally {
            try {
                closeResources(statement, null);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }
    }

    @Override
    public BankAccount findByNumber(Integer number)
            throws IntrusiveException {
        String sql = "select id, number, availableMoney" +
                " from bankaccount" +
                " where number=?";
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        BankAccount bankAccount = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1, number);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                bankAccount = new BankAccount();
                bankAccount.setId(resultSet.getInt("id"));
                bankAccount.setNumber(resultSet.getInt("number"));
                bankAccount.setAvailableMoney(resultSet.getBigDecimal("availableMoney"));
            }
            return bankAccount;
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        } finally {
            try {
                closeResources(statement, resultSet);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }
    }

    @Override
    public void updateAvailableMoney(BankAccount bankAccount)
            throws IntrusiveException {
        String sql = "update bankaccount set availableMoney=? where id=?";
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setBigDecimal(1, bankAccount.getAvailableMoney());
            statement.setInt(2, bankAccount.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        } finally {
            try {
                closeResources(statement, null);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }
    }
}
