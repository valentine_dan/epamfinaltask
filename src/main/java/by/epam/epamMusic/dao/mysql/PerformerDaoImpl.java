package by.epam.epamMusic.dao.mysql;

import by.epam.epamMusic.dao.PerformerDao;
import by.epam.epamMusic.entity.Performer;
import by.epam.epamMusic.entity.User;
import by.epam.epamMusic.exception.IntrusiveException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.ref.PhantomReference;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PerformerDaoImpl extends AbstractDaoImpl implements PerformerDao {
    private static Logger LOG = LogManager.getLogger(PerformerDaoImpl.class);

    @Override
    public List<Performer> read()
            throws IntrusiveException {
        String sql = "select id, name from performer order by name";
        List<Performer> performers = new ArrayList<>();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Performer performer = new Performer();
                String name = resultSet.getString("name");
                String id = resultSet.getString("id");
                performer.setId(Integer.valueOf(id));
                performer.setName(name);
                performers.add(performer);
            }
            return performers;
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        } finally {
            try {
                closeResources(statement, resultSet);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }
    }

    @Override
    public void create(Performer performer)
            throws IntrusiveException {
        String sql = "insert into performer (name) value (?)";
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setString(1, performer.getName());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        } finally {
            try {
                closeResources(statement, null);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }
    }

    @Override
    public Performer read(Integer id)
            throws IntrusiveException {
        String sql = "select name from performer where id=?";
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Performer performer = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                performer = new Performer();
                performer.setId(id);
                performer.setName(resultSet.getString("name"));
            }
            return performer;
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        } finally {
            try {
                closeResources(statement, resultSet);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }
    }

    @Override
    public void update(Performer entity)
            throws IntrusiveException {
        String sql = "update performer set name=? where id=?";
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setString(1, entity.getName());
            statement.setInt(2, entity.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        } finally {
            try {
                closeResources(statement, null);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }
    }

    @Override
    public void delete(Integer id)
            throws IntrusiveException {
        String sql = "delete from performer where id=?";
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new IntrusiveException(e);
        } finally {
            try {
                closeResources(statement, null);
            } catch (SQLException e) {
                LOG.error("Unable to close resources", e);
            }
        }
    }
}
