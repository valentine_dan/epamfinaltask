package by.epam.epamMusic.dao.mysql;

import by.epam.epamMusic.dao.PlaylistDao;
import by.epam.epamMusic.entity.Playlist;
import by.epam.epamMusic.exception.IntrusiveException;

import java.util.List;

public class PlaylistDaoImpl extends AbstractDaoImpl implements PlaylistDao {
    @Override
    public void create(Playlist entity) throws IntrusiveException {

    }

    @Override
    public Playlist read(Integer id) throws IntrusiveException {
        return null;
    }

    @Override
    public List<Playlist> read() throws IntrusiveException {
        return null;
    }

    @Override
    public void update(Playlist entity) throws IntrusiveException {

    }

    @Override
    public void delete(Integer id) throws IntrusiveException {

    }
}
