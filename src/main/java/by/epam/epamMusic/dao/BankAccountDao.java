package by.epam.epamMusic.dao;

import by.epam.epamMusic.entity.BankAccount;
import by.epam.epamMusic.exception.IntrusiveException;

public interface BankAccountDao extends Dao<BankAccount> {
    BankAccount findByNumber(Integer number)
            throws IntrusiveException;

    void updateAvailableMoney(BankAccount bankAccount)
            throws IntrusiveException;
}
