package by.epam.epamMusic.dao;

import by.epam.epamMusic.entity.User;
import by.epam.epamMusic.exception.IntrusiveException;

import java.util.List;

public interface UserDao extends Dao<User> {
    User findByLogin(String login)
            throws IntrusiveException;

    User findByLoginAndPassword(String login, String password)
            throws IntrusiveException;

    void buyTrack(Integer trackId,
                  Integer userId,
                  String paymentDate,
                  Integer paymentNumber)
            throws IntrusiveException;

    void updateBalance(User user)
            throws IntrusiveException;

    void addPaymentMethod(User user)
            throws IntrusiveException;
}
