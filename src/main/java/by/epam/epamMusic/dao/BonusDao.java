package by.epam.epamMusic.dao;

import by.epam.epamMusic.entity.Bonus;
import by.epam.epamMusic.exception.IntrusiveException;

import java.util.List;

public interface BonusDao extends Dao<Bonus> {
    List<Bonus> readByUserId(Integer userId)
            throws IntrusiveException;

    List<Bonus> readByType(Bonus.Type type)
            throws IntrusiveException;

    void addBonusToUser(Bonus bonus, Integer userId)
            throws IntrusiveException;

    Bonus readByDiscountValue(Double value)
            throws IntrusiveException;

    void writeOff(Integer bonusId, Integer userId)
            throws IntrusiveException;
}
