package by.epam.epamMusic.dao;

import by.epam.epamMusic.entity.Entity;
import by.epam.epamMusic.exception.IntrusiveException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public interface Dao<T extends Entity> {
    void create(T entity) throws IntrusiveException;

    T read(Integer id) throws IntrusiveException;

    List<T> read() throws IntrusiveException;

    void update(T entity) throws IntrusiveException;

    void delete(Integer id) throws IntrusiveException;

    default void closeResources(Statement statement, ResultSet resultSet)
            throws SQLException {
        if (statement != null) {
            statement.close();
        }
        if (resultSet != null) {
            resultSet.close();
        }
    }
}
