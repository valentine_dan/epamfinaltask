package by.epam.epamMusic.dao;

import by.epam.epamMusic.exception.IntrusiveException;

import java.sql.SQLException;

public interface DaoFactory {
    <T extends Dao<?>> T createDataAccessObject(DaoType type) throws IntrusiveException;
    void close();
}
