package by.epam.epamMusic.dao;

import by.epam.epamMusic.entity.Album;
import by.epam.epamMusic.exception.IntrusiveException;

import java.util.List;

public interface AlbumDao extends Dao<Album> {
    List<Album> findByPerformerId(Integer performerId)
            throws IntrusiveException;
}
