package by.epam.epamMusic.entity;

public enum Role {
    ADMIN,
    USER;

    public Integer getIdentity() {
        return ordinal();
    }
    public static Role getById(Integer id) {
        return Role.values()[id];
    }
}
