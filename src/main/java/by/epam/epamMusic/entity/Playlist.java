package by.epam.epamMusic.entity;

public class Playlist extends Entity {
    String genre;

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }
}
