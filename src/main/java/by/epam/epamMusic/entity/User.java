package by.epam.epamMusic.entity;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class User extends Entity {
    private Role role;
    private String login;
    private String password;
    private String name;
    private String surname;
    private String email;
    private BankAccount bankAccount;
    private BigDecimal balance;
    private Cart cart = new Cart();
    private List<Bonus> bonuses;

    private Set<Track> tracks;

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

    public Set<Track> getTracks() {
        return tracks;
    }

    public void setTracks(Set<Track> tracks) {
        this.tracks = tracks;
    }

    public List<Bonus> getBonuses() {
        return bonuses;
    }

    public void setBonuses(List<Bonus> bonuses) {
        this.bonuses = bonuses;
    }

    public static class Cart {
        private Set<Track> tracks = new HashSet<>();
        private Set<Performer> performers = new HashSet<>();
        private Set<Album> albums = new HashSet<>();
        private Set<Track> freeTracks = new HashSet<>();
        private BigDecimal totalPrice = new BigDecimal(0);

        public Set<Track> getTracks() {
            return tracks;
        }

        public void setTracks(Set<Track> tracks) {
            this.tracks = tracks;
        }

        public Set<Performer> getPerformers() {
            return performers;
        }

        public void setPerformers(Set<Performer> performers) {
            this.performers = performers;
        }

        public Set<Album> getAlbums() {
            return albums;
        }

        public void setAlbums(Set<Album> albums) {
            this.albums = albums;
        }

        public BigDecimal getTotalPrice() {
            return totalPrice;
        }

        public void setTotalPrice(BigDecimal totalPrice) {
            this.totalPrice = totalPrice;
        }

        public void put(Track track) {
            tracks.add(track);
        }

        public void put(Performer performer) {
            performers.add(performer);
        }

        public void put(Album album) {
            albums.add(album);
        }

        public Boolean isInCart(Track track) {
            return tracks.contains(track);
        }

        public Boolean isInCart(Performer performer) {
            return performers.contains(performer);
        }

        public Boolean isInCart(Album album) {
            return albums.contains(album);
        }

        public Set<Track> getFreeTracks() {
            return freeTracks;
        }

        public void setFreeTracks(Set<Track> freeTracks) {
            this.freeTracks = freeTracks;
        }
    }
}
