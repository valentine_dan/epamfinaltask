package by.epam.epamMusic.entity;

public class PaymentNumberCounter {
    private static Integer counter = 0;
    public static Integer generateNext(){
        return ++counter;
    }
}
