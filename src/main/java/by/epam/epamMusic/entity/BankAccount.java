package by.epam.epamMusic.entity;

import java.math.BigDecimal;

public class BankAccount extends Entity {
    private Integer number;
    private BigDecimal availableMoney;

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public BigDecimal getAvailableMoney() {
        return availableMoney;
    }

    public void setAvailableMoney(BigDecimal availableMoney) {
        this.availableMoney = availableMoney;
    }
}
