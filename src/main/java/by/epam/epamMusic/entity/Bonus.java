package by.epam.epamMusic.entity;

public class Bonus extends Entity {
    private double discountValue;
    private Type bonusType;

    public double getDiscountValue() {
        return discountValue;
    }

    public void setDiscountValue(double discountValue) {
        this.discountValue = discountValue;
    }

    public Type getBonusType() {
        return bonusType;
    }

    public void setBonusType(Type bonusType) {
        this.bonusType = bonusType;
    }

    public enum Type {
        DISCOUNT,
        FREE_TRACK
    }
}
