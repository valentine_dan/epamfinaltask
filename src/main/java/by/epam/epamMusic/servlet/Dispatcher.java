package by.epam.epamMusic.servlet;

import by.epam.epamMusic.command.Command;
import by.epam.epamMusic.command.CommandFactory;
import by.epam.epamMusic.command.CommandType;
import by.epam.epamMusic.command.RedirectionInfo;
import by.epam.epamMusic.dao.ConnectionPool;
import by.epam.epamMusic.exception.IntrusiveException;
import by.epam.epamMusic.exception.InvalidDataException;
import by.epam.epamMusic.service.mysql.ServiceManagerImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Dispatcher extends HttpServlet {
    private static Logger LOG = LogManager.getLogger(Dispatcher.class);

    @Override
    public void init() {
        try {
            ConnectionPool.getInstance().initializePool();
        } catch (IntrusiveException e) {
            LOG.fatal(e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        try {
            CommandType commandType = (CommandType) req.getAttribute("commandType");
            HttpSession session = req.getSession(false);
            RedirectionInfo info = null;
            if (commandType != null) {
                CommandFactory factory = new CommandFactory();
                factory.setServiceManager(new ServiceManagerImpl());
                Command command = factory.getCommand(commandType);
                info = command.execute(req, resp);
                factory.close();
                if (info.getUrl().endsWith(".jsp")) {
                    if (session != null) {
                        Map<String, Object> attributes = info.getAttributes();
                        for (String key :
                                attributes.keySet()) {
                            req.setAttribute(key, attributes.get(key));
                            session.removeAttribute(key);
                        }
                        if (req.getAttribute("message") == null) {
                            req.setAttribute("message", session.getAttribute("message"));
                        }
                        session.removeAttribute("message");
                    }
                    LOG.debug(String.format("Forward from '%s' to jsp '%s'", req.getRequestURI(), info.getUrl()));
                    req.getRequestDispatcher(info.getUrl()).forward(req, resp);
                } else if (info.getUrl().endsWith(".html")) {
                    if (session != null) {
                        Map<String, Object> attributes = info.getAttributes();
                        for (String key :
                                attributes.keySet()) {
                            session.setAttribute(key, attributes.get(key));
                        }
                    }
                    info.removeAttribute("message");
                    LOG.debug(String.format("Redirect from '%s' to '%s'", req.getRequestURI(), info.getUrl()));
                    resp.sendRedirect(req.getContextPath() + info.getUrl());
                } else if (info.getUrl().endsWith("/")) {
                    LOG.debug(String.format("Redirect from '%s' to '%s'", req.getRequestURI(), info.getUrl()));
                    resp.sendRedirect(req.getContextPath() + info.getUrl());
                } else {
                    LOG.error(String.format("Servlet can't process such request: %s", info.getUrl()));

                }
            }
        } catch (InvalidDataException e) {
            LOG.error("Invalid data was received from page", e);
            req.setAttribute("error", "Something went wrong and you were able to enter invalid data");
            req.getRequestDispatcher("/error.jsp").forward(req, resp);
        } catch (IntrusiveException e) {
            LOG.error(e);
        }
    }

    @Override
    public void destroy() {
        ConnectionPool.getInstance().free();
    }
}
