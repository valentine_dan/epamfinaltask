package by.epam.epamMusic.servlet.filter;

import by.epam.epamMusic.command.Command;
import by.epam.epamMusic.command.CommandFactory;
import by.epam.epamMusic.command.CommandType;
import by.epam.epamMusic.command.implementation.user.UserSignInImpl;
import by.epam.epamMusic.command.implementation.user.UserSignUpImpl;
import by.epam.epamMusic.entity.Role;
import by.epam.epamMusic.entity.User;
import by.epam.epamMusic.exception.IntrusiveException;
import by.epam.epamMusic.service.mysql.ServiceManagerImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public class SecurityFilter implements Filter {
    private static Logger LOG = LogManager.getLogger(SecurityFilter.class);

    @Override
    public void doFilter(ServletRequest servletRequest,
                         ServletResponse servletResponse,
                         FilterChain filterChain)
            throws IOException, ServletException {
        try {
            HttpServletRequest request = (HttpServletRequest) servletRequest;
            HttpServletResponse response = (HttpServletResponse) servletResponse;
            CommandFactory factory = new CommandFactory();
            factory.setServiceManager(new ServiceManagerImpl());
            CommandType commandType = (CommandType) request.getAttribute("commandType");
            Command command = factory.getCommand(commandType);
            HttpSession session = request.getSession();
            User user = null;
            if (session != null) {
                user = (User) session.getAttribute("user");
            }
            if (user != null) {
                List<Role> roles = command.getAccessPermit();
                boolean restricted = !roles.contains(user.getRole());
                if (restricted) {
                    LOG.info(String.format("User %s trying to access restricted resource", user.getLogin()));
                    request.setAttribute("message", "Access denied! You do not have permit to access this resource");
                    request.getServletContext().getRequestDispatcher("/error.jsp").forward(request, response);
                } else {
                    filterChain.doFilter(servletRequest, servletResponse);
                }
            } else {
                if (command.getClass().equals(UserSignInImpl.class)
                        || command.getClass().equals(UserSignUpImpl.class)) {
                    filterChain.doFilter(request, response);
                } else {
                    request.getServletContext().getRequestDispatcher("/signIn.jsp").forward(request, response);
                }
            }
        } catch (IntrusiveException e) {
            LOG.error(e);
        }
    }
}
