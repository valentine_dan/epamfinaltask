package by.epam.epamMusic.servlet.filter;

import by.epam.epamMusic.command.Command;
import by.epam.epamMusic.command.CommandFactory;
import by.epam.epamMusic.command.CommandType;
import by.epam.epamMusic.dao.ConnectionPool;
import by.epam.epamMusic.exception.IntrusiveException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class UriFilter implements Filter {
    private static Logger LOG = LogManager.getLogger(UriFilter.class);
    private static Map<String, CommandType> commands = new HashMap<>();

    static {
        commands.put("/", CommandType.DEFAULT);
        commands.put("/signOut", CommandType.SIGN_OUT);
        commands.put("/signIn", CommandType.SIGN_IN);
        commands.put("/signUp", CommandType.SIGN_UP);

        commands.put("/track/list", CommandType.TRACK_LIST);
        commands.put("/track/edit", CommandType.TRACK_EDIT);
        commands.put("/track/add", CommandType.TRACK_ADD);
        commands.put("/track/save", CommandType.TRACK_SAVE);
        commands.put("/track/delete", CommandType.TRACK_DELETE);

        commands.put("/performer/list", CommandType.PERFORMER_LIST);
        commands.put("/performer/edit", CommandType.PERFORMER_EDIT);
        commands.put("/performer/add", CommandType.PERFORMER_ADD);
        commands.put("/performer/save", CommandType.PERFORMER_SAVE);
        commands.put("/performer/delete", CommandType.PERFORMER_DELETE);

        commands.put("/album/list", CommandType.ALBUM_LIST);
        commands.put("/album/edit", CommandType.ALBUM_EDIT);
        commands.put("/album/add", CommandType.ALBUM_ADD);
        commands.put("/album/save", CommandType.ALBUM_SAVE);
        commands.put("/album/delete", CommandType.ALBUM_DELETE);

        commands.put("/user/buy", CommandType.BUY);
        commands.put("/user/list", CommandType.USER_LIST);
        commands.put("/user/index", CommandType.LOAD_MEDIA);
        commands.put("/user/profile", CommandType.PROFILE);
        commands.put("/user/cart", CommandType.CART);
        commands.put("/user/passwordChanging", CommandType.PASSWORD_CHANGE);
        commands.put("/user/emailChanging", CommandType.EMAIL_CHANGE);
        commands.put("/user/updateBalance", CommandType.BALANCE_TOP_UP);
        commands.put("/user/addPaymentMethod", CommandType.ADD_PAYMENT_METHOD);
        commands.put("/user/trackAddToCart", CommandType.TRACK_ADD_TO_CART);
        commands.put("/user/trackRemoveFromCart", CommandType.TRACK_REMOVE_FROM_CART);
        commands.put("/user/albumAddToCart", CommandType.ALBUM_ADD_TO_CART);
        commands.put("/user/albumRemoveFromCart", CommandType.ALBUM_REMOVE_FROM_CART);
        commands.put("/user/getFreeTrack", CommandType.FREE_TRACK_GET);

        commands.put("/admin/addFreeTrack", CommandType.FREE_TRACK_ADD);
        commands.put("/admin1/addDiscount", CommandType.DISCOUNT_ADD);
    }

    @Override
    public void doFilter(ServletRequest servletRequest,
                         ServletResponse servletResponse,
                         FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        String contextPath = request.getContextPath();
        String uri = request.getRequestURI();
        int begin = contextPath.length();
        int end = uri.lastIndexOf('.');
        String commandName;
        if (end >= 0) {
            commandName = uri.substring(begin, end);
        } else {
            commandName = uri.substring(begin);
        }
        CommandType type = commands.get(commandName);
        request.setAttribute("commandType", type);
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
