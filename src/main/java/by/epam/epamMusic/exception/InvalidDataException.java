package by.epam.epamMusic.exception;

public class InvalidDataException extends Exception {
    public InvalidDataException(String data, String value, String message) {
        super(String.format("Invalid parameter '%s': %s - %s", data, value, message));
    }
}
