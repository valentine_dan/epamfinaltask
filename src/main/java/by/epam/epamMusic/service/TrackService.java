package by.epam.epamMusic.service;

import by.epam.epamMusic.dao.DaoType;
import by.epam.epamMusic.dao.TrackDao;
import by.epam.epamMusic.dao.mysql.TrackDaoImpl;
import by.epam.epamMusic.entity.Album;
import by.epam.epamMusic.entity.Track;
import by.epam.epamMusic.exception.IntrusiveException;
import by.epam.epamMusic.exception.InvalidDataException;
import by.epam.epamMusic.validator.TrackValidator;

import java.math.BigDecimal;
import java.util.List;

public class TrackService extends Service {

    public void add(String name,
                    BigDecimal price,
                    Integer albumId)
            throws IntrusiveException, InvalidDataException {
        if (!TrackValidator.validateName(name)) {
            throw new InvalidDataException("Track name", name, "");
        }
        if (!TrackValidator.validatePrice(price)) {
            throw new InvalidDataException("Track price", price.toString(), "");
        }
        TrackDao trackDao = factory.createDataAccessObject(DaoType.TRACK);
        Track track = new Track();
        track.setName(name);
        track.setPrice(price);
        Album album = new Album();
        album.setId(albumId);
        track.setAlbum(album);
        trackDao.create(track);
    }

    public void update(Integer id,
                       String name,
                       BigDecimal price,
                       Integer albumId)
            throws IntrusiveException {
        TrackDao trackDao = factory.createDataAccessObject(DaoType.TRACK);
        Track track = new Track();
        track.setId(id);
        track.setName(name);
        track.setPrice(price);
        Album album = new Album();
        album.setId(albumId);
        track.setAlbum(album);
        trackDao.update(track);
    }

    public void delete(Integer id)
            throws IntrusiveException {
        TrackDao trackDao = factory.createDataAccessObject(DaoType.TRACK);
        trackDao.delete(id);
    }

    public Track findById(Integer id)
            throws IntrusiveException {
        TrackDao trackDao = factory.createDataAccessObject(DaoType.TRACK);
        return trackDao.read(id);
    }

    public List<Track> findByAlbumId(Integer id)
            throws IntrusiveException {
        TrackDao trackDao = factory.createDataAccessObject(DaoType.TRACK);
        return trackDao.findByAlbumId(id);
    }

    public List<Track> findByPerformerId(Integer performerId)
            throws IntrusiveException {
        TrackDao trackDao = factory.createDataAccessObject(DaoType.TRACK);
        return trackDao.findByPerformerId(performerId);
    }

    public List<Track> findByUserId(Integer userId)
            throws IntrusiveException {
        TrackDao trackDao = factory.createDataAccessObject(DaoType.TRACK);
        return trackDao.findByUserId(userId);
    }

    public List<Track> readAllTracks()
            throws IntrusiveException {
        TrackDao trackDao = factory.createDataAccessObject(DaoType.TRACK);
        return trackDao.read();
    }

    public boolean isOwned(Integer trackId)
            throws IntrusiveException {
        TrackDao trackDao = factory.createDataAccessObject(DaoType.TRACK);
        return trackDao.isOwned(trackId);
    }

}
