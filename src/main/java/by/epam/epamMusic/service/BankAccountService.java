package by.epam.epamMusic.service;

import by.epam.epamMusic.dao.BankAccountDao;
import by.epam.epamMusic.dao.DaoType;
import by.epam.epamMusic.dao.mysql.BankAccountDaoImpl;
import by.epam.epamMusic.entity.BankAccount;
import by.epam.epamMusic.exception.IntrusiveException;

public class BankAccountService extends Service {
    public BankAccount findByNumber(Integer number)
            throws IntrusiveException {
        BankAccountDao bankAccountDao = factory.createDataAccessObject(DaoType.BANK_ACCOUNT);
        return bankAccountDao.findByNumber(number);
    }

    public void updateAvailableMoney(BankAccount bankAccount)
            throws IntrusiveException {
        BankAccountDaoImpl bankAccountDao = factory.createDataAccessObject(DaoType.BANK_ACCOUNT);
        bankAccountDao.updateAvailableMoney(bankAccount);
    }
}
