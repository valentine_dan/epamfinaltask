package by.epam.epamMusic.service;

import by.epam.epamMusic.dao.ConnectionPool;
import by.epam.epamMusic.dao.DaoFactory;
import by.epam.epamMusic.dao.ProxyConnection;
import by.epam.epamMusic.exception.IntrusiveException;

import java.sql.SQLException;

public abstract class Service {
    protected DaoFactory factory;

    public void setFactory(DaoFactory factory) {
        this.factory = factory;
    }
}
