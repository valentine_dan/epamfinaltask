package by.epam.epamMusic.service;

import by.epam.epamMusic.dao.AlbumDao;
import by.epam.epamMusic.dao.DaoType;
import by.epam.epamMusic.dao.PerformerDao;
import by.epam.epamMusic.dao.mysql.AlbumDaoImpl;
import by.epam.epamMusic.dao.mysql.PerformerDaoImpl;
import by.epam.epamMusic.entity.Album;
import by.epam.epamMusic.entity.Performer;
import by.epam.epamMusic.exception.IntrusiveException;
import by.epam.epamMusic.exception.InvalidDataException;
import by.epam.epamMusic.validator.AlbumValidator;

import java.util.List;

public class AlbumService extends Service {
    public void add(String name,
                    Integer year,
                    Integer performerId)
            throws IntrusiveException, InvalidDataException {
        if(!AlbumValidator.validateName(name)){
            throw new InvalidDataException("Album", name, "");
        }
        if(!AlbumValidator.validateYear(year)){
            throw new InvalidDataException("Album year", String.valueOf(year), "");
        }
        AlbumDao albumDao = factory.createDataAccessObject(DaoType.ALBUM);
        Album album = new Album();
        album.setName(name);
        album.setYear(year);
        Performer performer = new Performer();
        performer.setId(performerId);
        album.setPerformer(performer);
        albumDao.create(album);
    }

    public void update(Integer id,
                       String name,
                       Integer year,
                       Integer performerId)
            throws IntrusiveException {
        AlbumDao albumDao = factory.createDataAccessObject(DaoType.ALBUM);
        PerformerDao performerDao = factory.createDataAccessObject(DaoType.PERFORMER);
        Album album = new Album();
        album.setId(id);
        album.setName(name);
        album.setYear(year);
        album.setPerformer(performerDao.read(performerId));
        albumDao.update(album);
    }

    public void delete(Integer id)
            throws IntrusiveException {
        AlbumDao albumDao = factory.createDataAccessObject(DaoType.ALBUM);
        albumDao.delete(id);
    }

    public List<Album> findByPerformerId(Integer performerId)
            throws IntrusiveException {
        AlbumDao albumDao = factory.createDataAccessObject(DaoType.ALBUM);
        return albumDao.findByPerformerId(performerId);
    }

    public Album findById(Integer id)
            throws IntrusiveException {
        AlbumDao albumDao = factory.createDataAccessObject(DaoType.ALBUM);
        return albumDao.read(id);
    }

    public List<Album> readAllAlbums()
            throws IntrusiveException {
        AlbumDao albumDao = factory.createDataAccessObject(DaoType.ALBUM);
        return albumDao.read();
    }
}
