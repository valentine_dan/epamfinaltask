package by.epam.epamMusic.service;

public enum ServiceType {
    ADMIN,
    USER,
    TRACK,
    ALBUM,
    PERFORMER,
    BANK_ACCOUNT,
    BONUS
}
