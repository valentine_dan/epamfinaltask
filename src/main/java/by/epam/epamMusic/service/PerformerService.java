package by.epam.epamMusic.service;

import by.epam.epamMusic.dao.DaoType;
import by.epam.epamMusic.dao.PerformerDao;
import by.epam.epamMusic.dao.mysql.PerformerDaoImpl;
import by.epam.epamMusic.entity.Performer;
import by.epam.epamMusic.exception.IntrusiveException;
import by.epam.epamMusic.exception.InvalidDataException;
import by.epam.epamMusic.validator.PerformerValidator;

import java.util.List;

public class PerformerService extends Service {
    public void delete(Integer id)
            throws IntrusiveException {
        PerformerDao performerDao = factory.createDataAccessObject(DaoType.PERFORMER);
        performerDao.delete(id);
    }

    public void update(Integer id, String name)
            throws IntrusiveException {
        PerformerDao performerDao = factory.createDataAccessObject(DaoType.PERFORMER);
        Performer performer = new Performer();
        performer.setId(id);
        performer.setName(name);
        performerDao.update(performer);
    }

    public Performer findById(Integer id)
            throws IntrusiveException {
        PerformerDao performerDao = factory.createDataAccessObject(DaoType.PERFORMER);
        return performerDao.read(id);
    }

    public List<Performer> readAllPerformers()
            throws IntrusiveException {
        PerformerDao performerDao = factory.createDataAccessObject(DaoType.PERFORMER);
        return performerDao.read();
    }

    public void add(String name)
            throws IntrusiveException, InvalidDataException {
        if (!PerformerValidator.validateName(name)) {
            throw new InvalidDataException("Performer name", name, "");
        }
        PerformerDao performerDao = factory.createDataAccessObject(DaoType.PERFORMER);
        Performer performer = new Performer();
        performer.setName(name);
        performerDao.create(performer);
    }
}
