package by.epam.epamMusic.service;

import by.epam.epamMusic.dao.DaoFactory;
import by.epam.epamMusic.exception.IntrusiveException;

import java.sql.SQLException;

public abstract class AbstractServiceManager {
    protected DaoFactory factory;

    public abstract <Type extends Service> Type createService(ServiceType serviceType)
            throws IntrusiveException;

    public void close() {
        factory.close();
    }
}
