package by.epam.epamMusic.service;

import by.epam.epamMusic.dao.BonusDao;
import by.epam.epamMusic.dao.DaoType;
import by.epam.epamMusic.dao.UserDao;
import by.epam.epamMusic.dao.mysql.BonusDaoImpl;
import by.epam.epamMusic.dao.mysql.UserDaoImpl;
import by.epam.epamMusic.entity.Role;
import by.epam.epamMusic.entity.User;
import by.epam.epamMusic.exception.InvalidDataException;
import by.epam.epamMusic.exception.IntrusiveException;
import by.epam.epamMusic.validator.FormValidator;

import java.util.List;

public class UserService extends Service {

    private boolean doesExist(String login)
            throws IntrusiveException {
        UserDao userDao = factory.createDataAccessObject(DaoType.USER);
        User user = userDao.findByLogin(login);
        return user != null;
    }

    public void registerUser(String login,
                             String password,
                             String email,
                             String name,
                             String surname)
            throws InvalidDataException, IntrusiveException {
        if (!FormValidator.isLoginValid(login)) {
            throw new InvalidDataException("login", login, "");
        }
        if (!FormValidator.isPasswordValid(password)) {
            throw new InvalidDataException("password", password, "");
        }
        if (!FormValidator.isEmailValid(email)) {
            throw new InvalidDataException("email", email, "");
        }
        if (doesExist(login)) {
            throw new InvalidDataException("login", login, "Login is not unique");
        }
        UserDao userDao = factory.createDataAccessObject(DaoType.USER);
        User user = new User();
        user.setRole(Role.getById(1));
        user.setPassword(password);
        user.setLogin(login);
        user.setEmail(email);
        user.setName(name);
        user.setSurname(surname);
        userDao.create(user);
    }


    public User findByLoginAndPassword(String login, String password)
            throws IntrusiveException {
        UserDao userDao = factory.createDataAccessObject(DaoType.USER);
        BonusDao bonusDao = factory.createDataAccessObject(DaoType.BONUS);
        User user = userDao.findByLoginAndPassword(login, password);
        if (user != null) {
            user.setBonuses(bonusDao.readByUserId(user.getId()));
        }
        return user;
    }

    public void update(User user)
            throws IntrusiveException {
        UserDao userDao = factory.createDataAccessObject(DaoType.USER);
        userDao.update(user);
    }

    public void addPaymentMethod(User user)
            throws IntrusiveException {
        UserDao userDao = factory.createDataAccessObject(DaoType.USER);
        userDao.addPaymentMethod(user);
    }

    public void updateBalance(User user)
            throws IntrusiveException {
        UserDao userDao = factory.createDataAccessObject(DaoType.USER);
        userDao.updateBalance(user);
    }

    public void buyTrack(Integer trackId,
                         Integer userId,
                         String paymentDate,
                         Integer paymentNumber) throws IntrusiveException {
        UserDao userDao = factory.createDataAccessObject(DaoType.USER);
        userDao.buyTrack(trackId, userId, paymentDate, paymentNumber);
    }

    public List<User> readAllUsers()
            throws IntrusiveException {
        UserDao userDao = factory.createDataAccessObject(DaoType.USER);
        BonusDao bonusDao = factory.createDataAccessObject(DaoType.BONUS);
        List<User> users = userDao.read();
        for (User user :
                users) {
            user.setBonuses(bonusDao.
                    readByUserId(user.getId()));
        }
        return users;
    }

    public User findByLogin(String login)
            throws IntrusiveException {
        UserDao userDao = factory.createDataAccessObject(DaoType.USER);
        return userDao.findByLogin(login);
    }
}
