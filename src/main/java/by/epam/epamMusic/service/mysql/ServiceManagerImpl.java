package by.epam.epamMusic.service.mysql;

import by.epam.epamMusic.dao.DaoFactory;
import by.epam.epamMusic.dao.mysql.DaoFactoryImpl;
import by.epam.epamMusic.exception.IntrusiveException;
import by.epam.epamMusic.service.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.HashMap;

public class ServiceManagerImpl extends AbstractServiceManager{
    private static HashMap<ServiceType, Service> services = new HashMap<>();

    public ServiceManagerImpl() throws IntrusiveException {
        factory = new DaoFactoryImpl();
    }

    static {
        services.put(ServiceType.USER, new UserService());
        services.put(ServiceType.TRACK, new TrackService());
        services.put(ServiceType.ALBUM, new AlbumService());
        services.put(ServiceType.PERFORMER, new PerformerService());
        services.put(ServiceType.BANK_ACCOUNT, new BankAccountService());
        services.put(ServiceType.BONUS, new BonusService());
    }

    public <Type extends Service> Type createService(ServiceType serviceType) throws IntrusiveException {
        @SuppressWarnings("unchecked")
        Type service = (Type) services.get(serviceType);
        if (service != null) {
            service.setFactory(factory);
            return service;
        }
        throw new IntrusiveException("It is impossible to create service.");
    }
}
