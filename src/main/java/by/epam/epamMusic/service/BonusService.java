package by.epam.epamMusic.service;

import by.epam.epamMusic.dao.BonusDao;
import by.epam.epamMusic.dao.DaoType;
import by.epam.epamMusic.dao.mysql.BonusDaoImpl;
import by.epam.epamMusic.entity.Bonus;
import by.epam.epamMusic.exception.IntrusiveException;

import java.util.List;

public class BonusService extends Service {
    public void addToUser(Bonus bonus, Integer userId)
            throws IntrusiveException {
        BonusDao bonusDao = factory.createDataAccessObject(DaoType.BONUS);
        bonusDao.addBonusToUser(bonus, userId);
    }

    public Bonus findByDiscountValue(Double value)
            throws IntrusiveException {
        BonusDao bonusDao = factory.createDataAccessObject(DaoType.BONUS);
        return bonusDao.readByDiscountValue(value);
    }

    public void add(Double value, Bonus.Type type)
            throws IntrusiveException {
        BonusDao bonusDao = factory.createDataAccessObject(DaoType.BONUS);
        Bonus bonus = new Bonus();
        bonus.setDiscountValue(value);
        bonus.setBonusType(type);
        bonusDao.create(bonus);
    }

    public List<Bonus> readByUserId(Integer userId)
            throws IntrusiveException {
        BonusDao bonusDao = factory.createDataAccessObject(DaoType.BONUS);
        return bonusDao.readByUserId(userId);
    }

    public List<Bonus> readByType(Bonus.Type type)
            throws IntrusiveException {
        BonusDao bonusDao = factory.createDataAccessObject(DaoType.BONUS);
        return bonusDao.readByType(type);
    }

    public void writeOff(Integer userId, Integer bonusId)
            throws IntrusiveException {
        BonusDao bonusDao = factory.createDataAccessObject(DaoType.BONUS);
        bonusDao.writeOff(bonusId, userId);
    }

    public Bonus findById(Integer bonusId)
            throws IntrusiveException {
        BonusDao bonusDao = factory.createDataAccessObject(DaoType.BONUS);
        return bonusDao.read(bonusId);
    }
}
