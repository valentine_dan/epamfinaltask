package by.epam.epamMusic.command;

import by.epam.epamMusic.entity.Role;
import by.epam.epamMusic.exception.IntrusiveException;
import by.epam.epamMusic.exception.InvalidDataException;
import by.epam.epamMusic.service.AbstractServiceManager;
import by.epam.epamMusic.service.mysql.ServiceManagerImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public abstract class Command {
    private List<Role> accessPermit;
    private String commandName;
    protected AbstractServiceManager manager;
    private RedirectionInfo info;

    public Command() {
    }

    public String getCommandName() {
        return commandName;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public RedirectionInfo getInfo() {
        return info;
    }

    public void setInfo(RedirectionInfo info) {
        this.info = info;
    }

    public List<Role> getAccessPermit() {
        return accessPermit;
    }

    public void setAccessPermit(List<Role> accessPermit) {
        this.accessPermit = accessPermit;
    }

    public void setManager(AbstractServiceManager manager) {
        this.manager = manager;
    }

    abstract public RedirectionInfo execute(HttpServletRequest request, HttpServletResponse response)
            throws IntrusiveException, InvalidDataException;
}
