package by.epam.epamMusic.command.implementation.user;

import by.epam.epamMusic.command.Command;
import by.epam.epamMusic.command.RedirectionInfo;
import by.epam.epamMusic.entity.Bonus;
import by.epam.epamMusic.entity.Role;
import by.epam.epamMusic.entity.User;
import by.epam.epamMusic.exception.IntrusiveException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.stream.Collectors;

public class UserCartImpl extends Command {
    public UserCartImpl() {
        setAccessPermit(List.of(Role.ADMIN, Role.USER));
    }

    @Override
    public RedirectionInfo execute(final HttpServletRequest request,
                                   final HttpServletResponse response)
            throws IntrusiveException {
        User user = (User) request.getSession().getAttribute("user");
        List<Bonus> freeTracks = user.getBonuses().stream().
                filter(bonus -> bonus.getBonusType().equals(Bonus.Type.FREE_TRACK)).
                collect(Collectors.toList());
        if (!freeTracks.isEmpty()) {
            request.setAttribute("freeTracks", freeTracks);
        }
        return new RedirectionInfo("/jsp/user/cart.jsp");
    }
}
