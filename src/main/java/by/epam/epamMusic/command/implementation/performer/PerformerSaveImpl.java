package by.epam.epamMusic.command.implementation.performer;

import by.epam.epamMusic.command.Command;
import by.epam.epamMusic.command.RedirectionInfo;
import by.epam.epamMusic.entity.Role;
import by.epam.epamMusic.exception.IntrusiveException;
import by.epam.epamMusic.service.PerformerService;
import by.epam.epamMusic.service.ServiceType;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class PerformerSaveImpl extends Command {
    public PerformerSaveImpl() {
        setAccessPermit(List.of(Role.ADMIN, Role.ADMIN));
    }

    @Override
    public RedirectionInfo execute(final HttpServletRequest request,
                                   final HttpServletResponse response)
            throws IntrusiveException {
        PerformerService service = manager.createService(ServiceType.PERFORMER);
        String name = request.getParameter("name");
        Integer id = Integer.valueOf(request.getParameter("id"));
        service.update(id, name);
        return new RedirectionInfo("/performer/list.html");
    }
}
