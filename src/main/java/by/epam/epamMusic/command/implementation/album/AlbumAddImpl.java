package by.epam.epamMusic.command.implementation.album;

import by.epam.epamMusic.command.Command;
import by.epam.epamMusic.command.RedirectionInfo;
import by.epam.epamMusic.entity.Role;
import by.epam.epamMusic.exception.IntrusiveException;
import by.epam.epamMusic.exception.InvalidDataException;
import by.epam.epamMusic.service.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;

public class AlbumAddImpl extends Command {
    private static Logger LOG = LogManager.getLogger(AlbumAddImpl.class);

    public AlbumAddImpl() throws IntrusiveException{
        setAccessPermit(List.of(Role.ADMIN));
    }

    @Override
    public RedirectionInfo execute(final HttpServletRequest request,
                                   final HttpServletResponse response)
            throws IntrusiveException {
        try {
            AlbumService service = manager.createService(ServiceType.ALBUM);
            String albumName = request.getParameter("albumName");
            Integer albumYear = Integer.valueOf(request.getParameter("albumYear"));
            Integer performerId = Integer.valueOf(request.getParameter("performer"));
            service.add(albumName, albumYear, performerId);
            LOG.info("New album was added");
            return new RedirectionInfo("/album/list.html");
        } catch (InvalidDataException e) {
            request.setAttribute("message", Arrays.toString(e.getStackTrace()));
            return new RedirectionInfo("/error.jsp");
        }
    }
}
