package by.epam.epamMusic.command.implementation.album;

import by.epam.epamMusic.utility.Utility;
import by.epam.epamMusic.command.Command;
import by.epam.epamMusic.command.RedirectionInfo;
import by.epam.epamMusic.entity.Album;
import by.epam.epamMusic.entity.Role;
import by.epam.epamMusic.entity.Track;
import by.epam.epamMusic.entity.User;
import by.epam.epamMusic.exception.IntrusiveException;
import by.epam.epamMusic.service.AlbumService;
import by.epam.epamMusic.service.ServiceType;
import by.epam.epamMusic.service.TrackService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

public class AlbumAddToCartImpl extends Command {
    public AlbumAddToCartImpl() {
        setAccessPermit(List.of(Role.ADMIN, Role.USER));
    }

    @Override
    public RedirectionInfo execute(final HttpServletRequest request,
                                   final HttpServletResponse response)
            throws IntrusiveException {
        TrackService trackService = manager.createService(ServiceType.TRACK);
        AlbumService albumService = manager.createService(ServiceType.ALBUM);
        Integer albumId = Integer.valueOf(request.getParameter("id"));
        User user = (User) request.getSession().getAttribute("user");
        Album album = albumService.findById(albumId);
        List<Track> tracksFromAlbum = trackService.findByAlbumId(albumId);
        List<Track> alreadyOwnedTracks = trackService.findByUserId(user.getId());
        Set<Track> tracksToBuy = Utility.intersection(tracksFromAlbum, alreadyOwnedTracks);
        if (!tracksToBuy.isEmpty()) {
            user.getCart().put(album);
            BigDecimal newTotalPrice = user.getCart().getTotalPrice();
            for (Track track :
                    tracksToBuy) {
                newTotalPrice = newTotalPrice.add(track.getPrice());
            }
            user.getCart().getTracks().addAll(tracksToBuy);
            user.getCart().setTotalPrice(newTotalPrice);
            request.getSession().setAttribute("user", user);
            return new RedirectionInfo("/album/list.html");
        } else {
            RedirectionInfo info = new RedirectionInfo("/album/list.html");
            info.setAttribute("message", "You have already bought all tracks from this album");
            return info;
        }

    }
}
