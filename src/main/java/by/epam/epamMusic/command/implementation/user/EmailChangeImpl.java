package by.epam.epamMusic.command.implementation.user;

import by.epam.epamMusic.command.Command;
import by.epam.epamMusic.command.RedirectionInfo;
import by.epam.epamMusic.entity.Role;
import by.epam.epamMusic.entity.User;
import by.epam.epamMusic.exception.IntrusiveException;
import by.epam.epamMusic.service.ServiceType;
import by.epam.epamMusic.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class EmailChangeImpl extends Command {
    private static Logger LOG = LogManager.getLogger(EmailChangeImpl.class);

    public EmailChangeImpl() {
        setAccessPermit(List.of(Role.ADMIN, Role.USER));
    }

    @Override
    public RedirectionInfo execute(final HttpServletRequest request,
                                   final HttpServletResponse response)
            throws IntrusiveException {
        UserService userService = manager.createService(ServiceType.USER);
        String oldEmail = request.getParameter("old-email");
        String newEmail = request.getParameter("new-email");
        User userInSession = (User) request.getSession().getAttribute("user");
        RedirectionInfo info = new RedirectionInfo("/user/profile.html");
        if (oldEmail != null && newEmail != null) {
            if (userInSession.getEmail().equals(oldEmail)) {
                userInSession.setEmail(newEmail);
                request.getSession().setAttribute("user", userInSession);
                userService.update(userInSession);
                request.getSession().setAttribute("message", "E-Mail was successfully changed!");
                LOG.info(String.format("User %s successfully changed E-Mail", userInSession.getLogin()));
                return info;
            } else {
                info.setAttribute("message", "You've entered invalid old E-Mail");
                return info;
            }
        } else {
            info.setAttribute("message", "Invalid E-Mail");
            return info;
        }
    }
}
