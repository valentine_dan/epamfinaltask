package by.epam.epamMusic.command.implementation.performer;

import by.epam.epamMusic.command.Command;
import by.epam.epamMusic.command.RedirectionInfo;
import by.epam.epamMusic.entity.Performer;
import by.epam.epamMusic.entity.Role;
import by.epam.epamMusic.exception.IntrusiveException;
import by.epam.epamMusic.service.PerformerService;
import by.epam.epamMusic.service.ServiceType;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class PerformerListImpl extends Command {
    private static final Integer itemsPerPage = 10;

    public PerformerListImpl() {
        setAccessPermit(List.of(Role.ADMIN, Role.USER));
    }

    @Override
    public RedirectionInfo execute(final HttpServletRequest request,
                                   final HttpServletResponse response)
            throws IntrusiveException {
        PerformerService performerService = manager.createService(ServiceType.PERFORMER);
        List<Performer> performers = performerService.readAllPerformers();
        request.setAttribute("performers", performers);
        Integer numberOfPages = (performers.size() / itemsPerPage) + 1;
        if (request.getParameter("page") != null) {
            Integer currentPage = Integer.valueOf(request.getParameter("page"));
            request.setAttribute("currentPage", currentPage);
            request.getSession().setAttribute("currentPage", currentPage);
            request.setAttribute("numberOfPages", numberOfPages);
            request.setAttribute("itemsPerPage", itemsPerPage);
        } else {
            Integer currentPage = (Integer) request.getSession().getAttribute("currentPage");
            request.getSession().removeAttribute("currentPage");
            request.getSession().setAttribute("currentPage", currentPage);
            request.setAttribute("numberOfPages", numberOfPages);
            request.setAttribute("itemsPerPage", itemsPerPage);
        }
        return new RedirectionInfo("/jsp/performer/list.jsp");
    }
}
