package by.epam.epamMusic.command.implementation.user;

import by.epam.epamMusic.command.Command;
import by.epam.epamMusic.command.RedirectionInfo;
import by.epam.epamMusic.entity.Role;
import by.epam.epamMusic.exception.IntrusiveException;
import by.epam.epamMusic.exception.InvalidDataException;
import by.epam.epamMusic.service.ServiceType;
import by.epam.epamMusic.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class UserSignUpImpl extends Command {
    private static Logger LOG = LogManager.getLogger(UserSignUpImpl.class);

    public UserSignUpImpl() {
        setAccessPermit(List.of(Role.ADMIN, Role.USER));
    }

    @Override
    public RedirectionInfo execute(HttpServletRequest request, HttpServletResponse response)
            throws IntrusiveException, InvalidDataException {
        UserService service = manager.createService(ServiceType.USER);
        String login = request.getParameter("login");
        String email = request.getParameter("email");
        String name = request.getParameter("name");
        String surname = request.getParameter("surname");
        String password = request.getParameter("password");
        service.registerUser(login, password, email, name, surname);
        LOG.info(String.format("New user signed up: %s", login));
        return new RedirectionInfo("/signIn.jsp");
    }


}
