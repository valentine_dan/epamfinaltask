package by.epam.epamMusic.command.implementation.track;

import by.epam.epamMusic.command.Command;
import by.epam.epamMusic.command.RedirectionInfo;
import by.epam.epamMusic.entity.Role;
import by.epam.epamMusic.exception.IntrusiveException;
import by.epam.epamMusic.service.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class TrackEditImpl extends Command {
    public TrackEditImpl() {
        setAccessPermit(List.of(Role.ADMIN));
    }

    @Override
    public RedirectionInfo execute(final HttpServletRequest request,
                                   final HttpServletResponse response)
            throws IntrusiveException {
        PerformerService performerService = manager.createService(ServiceType.PERFORMER);
        AlbumService albumService = manager.createService(ServiceType.ALBUM);
        TrackService trackService = manager.createService(ServiceType.TRACK);
        Integer trackId = Integer.valueOf(request.getParameter("id"));
        request.setAttribute("track", trackService.findById(trackId));
        request.setAttribute("performers", performerService.readAllPerformers());
        request.setAttribute("albums", albumService.readAllAlbums());
        return new RedirectionInfo("/jsp/track/edit.jsp");
    }
}
