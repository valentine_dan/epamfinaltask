package by.epam.epamMusic.command.implementation.track;

import by.epam.epamMusic.command.Command;
import by.epam.epamMusic.command.RedirectionInfo;
import by.epam.epamMusic.entity.Role;
import by.epam.epamMusic.entity.Track;
import by.epam.epamMusic.entity.User;
import by.epam.epamMusic.exception.IntrusiveException;
import by.epam.epamMusic.service.ServiceType;
import by.epam.epamMusic.service.TrackService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.List;

public class TrackAddToCart extends Command {
    public TrackAddToCart() {
        setAccessPermit(List.of(Role.ADMIN, Role.USER));
    }

    @Override
    public RedirectionInfo execute(final HttpServletRequest request,
                                   final HttpServletResponse response)
            throws IntrusiveException {
        TrackService trackService = manager.createService(ServiceType.TRACK);
        Integer trackId = Integer.valueOf(request.getParameter("id"));
        Track track = trackService.findById(trackId);
        User user = (User) request.getSession().getAttribute("user");
        user.getCart().put(track);
        BigDecimal newTotalPrice = user.getCart().getTotalPrice().add(track.getPrice());
        user.getCart().setTotalPrice(newTotalPrice);
        request.getSession().setAttribute("user", user);
        Integer currentPage = (Integer) request.getSession().getAttribute("currentPage");
        request.getSession().removeAttribute("currentPage");
        RedirectionInfo info = new RedirectionInfo("/track/list.html");
        info.setAttribute("currentPage", currentPage);
        return info;
    }
}
