package by.epam.epamMusic.command.implementation.performer;

import by.epam.epamMusic.command.Command;
import by.epam.epamMusic.command.RedirectionInfo;
import by.epam.epamMusic.entity.Role;
import by.epam.epamMusic.exception.IntrusiveException;
import by.epam.epamMusic.exception.InvalidDataException;
import by.epam.epamMusic.service.PerformerService;
import by.epam.epamMusic.service.ServiceType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;

public class PerformerAddImpl extends Command {
    private static Logger LOG = LogManager.getLogger(PerformerAddImpl.class);

    public PerformerAddImpl() {
        setAccessPermit(List.of(Role.ADMIN));
    }

    @Override
    public RedirectionInfo execute(final HttpServletRequest request,
                                   final HttpServletResponse response)
            throws IntrusiveException, InvalidDataException {
        try {
            PerformerService service = manager.createService(ServiceType.PERFORMER);
            String performerName = request.getParameter("performerName");
            service.add(performerName);
            RedirectionInfo info = new RedirectionInfo("/performer/list.html");
            info.setAttribute("performers", service.readAllPerformers());
            LOG.info("New performer was added");
            return info;
        } catch (InvalidDataException e) {
            request.setAttribute("message", Arrays.toString(e.getStackTrace()));
            return new RedirectionInfo("/error.jsp");
        }
    }
}
