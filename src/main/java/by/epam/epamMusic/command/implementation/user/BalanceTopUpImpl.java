package by.epam.epamMusic.command.implementation.user;

import by.epam.epamMusic.command.Command;
import by.epam.epamMusic.command.RedirectionInfo;
import by.epam.epamMusic.entity.Role;
import by.epam.epamMusic.entity.User;
import by.epam.epamMusic.exception.IntrusiveException;
import by.epam.epamMusic.service.BankAccountService;
import by.epam.epamMusic.service.ServiceType;
import by.epam.epamMusic.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.List;

public class BalanceTopUpImpl extends Command {
    public BalanceTopUpImpl() {
        setAccessPermit(List.of(Role.ADMIN, Role.USER));
    }

    @Override
    public RedirectionInfo execute(final HttpServletRequest request,
                                   final HttpServletResponse response)
            throws IntrusiveException {
        BigDecimal moneyAmount = new BigDecimal(request.getParameter("moneyAmount"));
        User user = (User) request.getSession().getAttribute("user");

        if (user.getBankAccount().getAvailableMoney().compareTo(moneyAmount) < 0) {
            RedirectionInfo info = new RedirectionInfo("/user/profile.html");
            info.setAttribute("message", "Sorry, you do not have enough money on your bank account");
            return info;
        } else {
            UserService userService = manager.createService(ServiceType.USER);
            BankAccountService bankAccountService = manager.createService(ServiceType.BANK_ACCOUNT);
            BigDecimal newAvailableMoney = user.getBankAccount().getAvailableMoney().subtract(moneyAmount);
            BigDecimal newBalance = user.getBalance().add(moneyAmount);
            user.getBankAccount().setAvailableMoney(newAvailableMoney);
            user.setBalance(newBalance);
            userService.updateBalance(user);
            bankAccountService.updateAvailableMoney(user.getBankAccount());
            request.getSession().setAttribute("user", user);
            return new RedirectionInfo("/user/profile.html");
        }
    }
}
