package by.epam.epamMusic.command.implementation.performer;

import by.epam.epamMusic.command.Command;
import by.epam.epamMusic.command.RedirectionInfo;
import by.epam.epamMusic.entity.Role;
import by.epam.epamMusic.exception.IntrusiveException;
import by.epam.epamMusic.service.PerformerService;
import by.epam.epamMusic.service.ServiceType;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class PerformerEditImpl extends Command {
    public PerformerEditImpl() {
        setAccessPermit(List.of(Role.ADMIN));
    }

    @Override
    public RedirectionInfo execute(final HttpServletRequest request,
                                   final HttpServletResponse response)
            throws IntrusiveException {
        PerformerService service = manager.createService(ServiceType.PERFORMER);
        Integer performerId = Integer.valueOf(request.getParameter("id"));
        request.setAttribute("performer", service.findById(performerId));
        return new RedirectionInfo("/jsp/performer/edit.jsp");
    }
}
