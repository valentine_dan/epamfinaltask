package by.epam.epamMusic.command.implementation.user;

import by.epam.epamMusic.command.Command;
import by.epam.epamMusic.command.RedirectionInfo;
import by.epam.epamMusic.entity.Role;
import by.epam.epamMusic.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class UserSignOutImpl extends Command {
    private static Logger LOG = LogManager.getLogger(UserSignOutImpl.class);

    public UserSignOutImpl() {
        setAccessPermit(List.of(Role.ADMIN, Role.USER));
    }

    @Override
    public RedirectionInfo execute(final HttpServletRequest request,
                                   final HttpServletResponse response) {
        User user = (User) request.getSession().getAttribute("user");
        if (user != null) {
            request.getSession().invalidate();
            LOG.info(String.format("User %s signed out", user.getLogin()));
        }
        return new RedirectionInfo("/");
    }
}
