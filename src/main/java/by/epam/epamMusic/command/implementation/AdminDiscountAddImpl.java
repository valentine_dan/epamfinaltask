package by.epam.epamMusic.command.implementation;

import by.epam.epamMusic.command.Command;
import by.epam.epamMusic.command.RedirectionInfo;
import by.epam.epamMusic.entity.Bonus;
import by.epam.epamMusic.entity.Role;
import by.epam.epamMusic.exception.IntrusiveException;
import by.epam.epamMusic.service.BonusService;
import by.epam.epamMusic.service.ServiceType;
import by.epam.epamMusic.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class AdminDiscountAddImpl extends Command {
    public AdminDiscountAddImpl() {
        setAccessPermit(List.of(Role.ADMIN));
    }

    @Override
    public RedirectionInfo execute(final HttpServletRequest request,
                                   final HttpServletResponse response)
            throws IntrusiveException {
        BonusService bonusService = manager.createService(ServiceType.BONUS);
        UserService userService = manager.createService(ServiceType.USER);
        Integer userId = Integer.valueOf(request.getParameter("id"));
        Double value = Double.valueOf(request.getParameter("discValue"));
        Bonus bonus = bonusService.findByDiscountValue(value);
        if (bonus == null) {
            bonusService.add(value, Bonus.Type.DISCOUNT);
            bonus = bonusService.findByDiscountValue(value);
        }
        bonusService.addToUser(bonus, userId);
        return new RedirectionInfo("/user/list.html");
    }
}
