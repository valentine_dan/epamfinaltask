package by.epam.epamMusic.command.implementation.track;

import by.epam.epamMusic.command.Command;
import by.epam.epamMusic.command.RedirectionInfo;
import by.epam.epamMusic.entity.Album;
import by.epam.epamMusic.entity.Role;
import by.epam.epamMusic.entity.Track;
import by.epam.epamMusic.entity.User;
import by.epam.epamMusic.exception.IntrusiveException;
import by.epam.epamMusic.service.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class TrackListImpl extends Command {
    public TrackListImpl() {
        setAccessPermit(List.of(Role.ADMIN, Role.USER));
    }

    private final static Integer itemsPerPage = 10;

    @Override
    public RedirectionInfo execute(final HttpServletRequest request,
                                   final HttpServletResponse response)
            throws IntrusiveException {
        TrackService trackService = manager.createService(ServiceType.TRACK);
        AlbumService albumService = manager.createService(ServiceType.ALBUM);
        User user = (User) request.getSession().getAttribute("user");
        if (user.getRole().equals(Role.ADMIN)) {
            List<Album> albums = albumService.readAllAlbums();
            request.setAttribute("albums", albums);
        }
        List<Track> ownedTracks = trackService.findByUserId(user.getId());
        List<Track> tracks;
        if (request.getParameter("performerId") != null) {
            Integer id = Integer.valueOf(request.getParameter("performerId"));
            tracks = trackService.findByPerformerId(id);
        } else if (request.getParameter("albumId") != null) {
            Integer id = Integer.valueOf(request.getParameter("albumId"));
            tracks = trackService.findByAlbumId(id);
        } else {
            tracks = trackService.readAllTracks();
        }
        request.setAttribute("tracks", tracks);
        request.setAttribute("ownedTracks", ownedTracks);
        Integer numberOfPages = (tracks.size() / itemsPerPage) + 1;
        if (request.getParameter("page") != null) {
            Integer currentPage = Integer.valueOf(request.getParameter("page"));
            request.getSession().setAttribute("currentPage", currentPage);
            request.setAttribute("numberOfPages", numberOfPages);
            request.setAttribute("itemsPerPage", itemsPerPage);
        } else {
            Integer currentPage = (Integer) request.getSession().getAttribute("currentPage");
            request.getSession().removeAttribute("currentPage");
            request.getSession().setAttribute("currentPage", currentPage);
            request.setAttribute("numberOfPages", numberOfPages);
            request.setAttribute("itemsPerPage", itemsPerPage);
        }
        return new RedirectionInfo("/jsp/track/list.jsp");
    }
}
