package by.epam.epamMusic.command.implementation.performer;

import by.epam.epamMusic.command.Command;
import by.epam.epamMusic.command.RedirectionInfo;
import by.epam.epamMusic.entity.Album;
import by.epam.epamMusic.entity.Performer;
import by.epam.epamMusic.entity.Role;
import by.epam.epamMusic.exception.IntrusiveException;
import by.epam.epamMusic.service.AlbumService;
import by.epam.epamMusic.service.PerformerService;
import by.epam.epamMusic.service.ServiceType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class PerformerDeleteImpl extends Command {
    private static Logger LOG = LogManager.getLogger(PerformerDeleteImpl.class);

    public PerformerDeleteImpl() {
        setAccessPermit(List.of(Role.ADMIN));
    }

    @Override
    public RedirectionInfo execute(final HttpServletRequest request,
                                   final HttpServletResponse response)
            throws IntrusiveException {
        PerformerService service = manager.createService(ServiceType.PERFORMER);
        AlbumService albumService = manager.createService(ServiceType.ALBUM);
        Integer id = Integer.valueOf(request.getParameter("id"));
        Performer performer = service.findById(id);
        List<Album> albums = albumService.findByPerformerId(id);
        RedirectionInfo info = new RedirectionInfo("/performer/list.html");
        if (!albums.isEmpty()) {
            info.setAttribute("message", "You can't delete performer '" + performer.getName() + "'");
            return info;
        } else {
            service.delete(id);
            LOG.info(String.format("Performer id=%d was deleted", id));
            return info;
        }
    }
}
