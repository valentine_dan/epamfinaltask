package by.epam.epamMusic.command.implementation.track;

import by.epam.epamMusic.command.Command;
import by.epam.epamMusic.command.RedirectionInfo;
import by.epam.epamMusic.entity.Role;
import by.epam.epamMusic.entity.Track;
import by.epam.epamMusic.entity.User;
import by.epam.epamMusic.exception.IntrusiveException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

public class TrackRemoveFromCartImpl extends Command {
    public TrackRemoveFromCartImpl() {
        setAccessPermit(List.of(Role.ADMIN, Role.USER));
    }

    @Override
    public RedirectionInfo execute(final HttpServletRequest request,
                                   final HttpServletResponse response)
            throws IntrusiveException {
        User user = (User) request.getSession().getAttribute("user");
        Set<Track> tracks = user.getCart().getTracks();
        Integer trackId = Integer.valueOf(request.getParameter("id"));
        Track trackToRemove = new Track();
        for (Track track :
                tracks) {
            if (track.getId().equals(trackId)) {
                trackToRemove = track;
                break;
            }
        }
        tracks.remove(trackToRemove);
        BigDecimal newTotalPrice = user.getCart().getTotalPrice().subtract(trackToRemove.getPrice());
        user.getCart().setTotalPrice(newTotalPrice);
        user.getCart().setTracks(tracks);
        request.getSession().setAttribute("user", user);
        return new RedirectionInfo("/user/cart.html");
    }
}
