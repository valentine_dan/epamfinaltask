package by.epam.epamMusic.command.implementation.track;

import by.epam.epamMusic.command.Command;
import by.epam.epamMusic.command.RedirectionInfo;
import by.epam.epamMusic.entity.Role;
import by.epam.epamMusic.exception.IntrusiveException;
import by.epam.epamMusic.service.ServiceType;
import by.epam.epamMusic.service.TrackService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.List;

public class TrackSaveImpl extends Command {
    public TrackSaveImpl() {
        setAccessPermit(List.of(Role.ADMIN));
    }

    @Override
    public RedirectionInfo execute(HttpServletRequest request, HttpServletResponse response)
            throws IntrusiveException {
        TrackService service = manager.createService(ServiceType.TRACK);
        Integer id = Integer.valueOf(request.getParameter("id"));
        String trackName = request.getParameter("name");
        BigDecimal price = new BigDecimal(request.getParameter("price"));
        Integer albumId = Integer.valueOf(request.getParameter("album"));
        service.update(id, trackName, price, albumId);
        Integer currentPage = (Integer) request.getSession().getAttribute("currentPage");
        request.getSession().removeAttribute("currentPage");
        RedirectionInfo info = new RedirectionInfo("/track/list.html");
        info.setAttribute("currentPage", currentPage);
        return info;
    }
}
