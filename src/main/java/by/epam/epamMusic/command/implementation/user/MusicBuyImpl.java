package by.epam.epamMusic.command.implementation.user;

import by.epam.epamMusic.command.Command;
import by.epam.epamMusic.command.RedirectionInfo;
import by.epam.epamMusic.entity.*;
import by.epam.epamMusic.exception.IntrusiveException;
import by.epam.epamMusic.service.BonusService;
import by.epam.epamMusic.service.ServiceType;
import by.epam.epamMusic.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

public class MusicBuyImpl extends Command {
    private static Logger LOG = LogManager.getLogger(MusicBuyImpl.class);

    public MusicBuyImpl() {
        setAccessPermit(List.of(Role.ADMIN, Role.USER));
    }

    @Override
    public RedirectionInfo execute(final HttpServletRequest request,
                                   final HttpServletResponse response)
            throws IntrusiveException {
        User user = (User) request.getSession().getAttribute("user");
        String discountId = request.getParameter("discount");
        BigDecimal totalPrice = user.getCart().getTotalPrice();
        if (discountId != null) {
            BonusService bonusService = manager.createService(ServiceType.BONUS);
            Bonus discount = bonusService.findById(Integer.valueOf(discountId));
            bonusService.writeOff(user.getId(), discount.getId());
            totalPrice = totalPrice.divide(new BigDecimal(discount.getDiscountValue()));
        }
        BigDecimal availableMoney = user.getBalance();
        if (totalPrice.compareTo(availableMoney) > 0) {
            RedirectionInfo info = new RedirectionInfo("/jsp/user/cart.jsp");
            request.setAttribute("message", "Sorry, you do not have enough money");
            return info;
        } else {
            BigDecimal restOfMoney = availableMoney.subtract(totalPrice);
            UserService userService = manager.createService(ServiceType.USER);
            Set<Track> tracks = user.getCart().getTracks();
            for (Track track :
                    tracks) {
                Date now = new Date();
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String currentDate = dateFormat.format(now);
                Integer paymentNumber = PaymentNumberCounter.generateNext();
                userService.buyTrack(track.getId(), user.getId(), currentDate, paymentNumber);
            }
            user.setBalance(restOfMoney);
            userService.updateBalance(user);
            user.setCart(new User.Cart());
            request.getSession().setAttribute("user", user);
            LOG.info("User %s successfully made a purchase", user.getLogin());
            return new RedirectionInfo("/user/profile.html");
        }
    }
}
