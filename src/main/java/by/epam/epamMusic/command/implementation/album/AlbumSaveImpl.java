package by.epam.epamMusic.command.implementation.album;

import by.epam.epamMusic.command.Command;
import by.epam.epamMusic.command.RedirectionInfo;
import by.epam.epamMusic.entity.Role;
import by.epam.epamMusic.exception.IntrusiveException;
import by.epam.epamMusic.service.AlbumService;
import by.epam.epamMusic.service.ServiceType;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class AlbumSaveImpl extends Command {
    public AlbumSaveImpl() {
        setAccessPermit(List.of(Role.ADMIN, Role.USER));
    }

    @Override
    public RedirectionInfo execute(final HttpServletRequest request,
                                   final HttpServletResponse response)
            throws IntrusiveException {
        AlbumService service = manager.createService(ServiceType.ALBUM);
        Integer id = Integer.valueOf(request.getParameter("id"));
        String albumName = request.getParameter("name");
        Integer year = Integer.valueOf(request.getParameter("year"));
        Integer performerId = Integer.valueOf(request.getParameter("performer"));
        service.update(id, albumName, year, performerId);
        return new RedirectionInfo("/album/list.html");
    }
}
