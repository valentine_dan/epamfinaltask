package by.epam.epamMusic.command.implementation.user;

import by.epam.epamMusic.command.Command;
import by.epam.epamMusic.command.RedirectionInfo;
import by.epam.epamMusic.entity.Bonus;
import by.epam.epamMusic.entity.Role;
import by.epam.epamMusic.entity.User;
import by.epam.epamMusic.exception.IntrusiveException;
import by.epam.epamMusic.service.BonusService;
import by.epam.epamMusic.service.ServiceType;
import by.epam.epamMusic.service.TrackService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.stream.Collectors;

public class UserProfileImpl extends Command {
    public UserProfileImpl() {
        setAccessPermit(List.of(Role.ADMIN, Role.USER));
    }

    @Override
    public RedirectionInfo execute(final HttpServletRequest request,
                                   final HttpServletResponse response)
            throws IntrusiveException {
        TrackService trackService = manager.createService(ServiceType.TRACK);
        BonusService bonusService = manager.createService(ServiceType.BONUS);
        User user = (User) request.getSession().getAttribute("user");
        List<Bonus> bonuses = bonusService.readByUserId(user.getId());
        user.setBonuses(bonuses);
        request.setAttribute("ownedTracks", trackService.findByUserId(user.getId()));
        Long freeTracksNum = bonuses.stream().
                filter(bonus -> bonus.getBonusType().equals(Bonus.Type.FREE_TRACK)).
                count();
        List<Bonus> discounts = bonuses.stream().
                filter(bonus -> bonus.getBonusType().equals(Bonus.Type.DISCOUNT)).
                collect(Collectors.toList());
        request.setAttribute("freeTracksNum", freeTracksNum);
        request.setAttribute("discounts", discounts);
        return new RedirectionInfo("/jsp/user/profile.jsp");
    }
}
