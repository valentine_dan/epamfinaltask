package by.epam.epamMusic.command.implementation.track;

import by.epam.epamMusic.command.Command;
import by.epam.epamMusic.command.RedirectionInfo;
import by.epam.epamMusic.entity.Role;
import by.epam.epamMusic.exception.IntrusiveException;
import by.epam.epamMusic.service.ServiceType;
import by.epam.epamMusic.service.TrackService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class TrackDeleteImpl extends Command {
    private static Logger LOG = LogManager.getLogger(TrackDeleteImpl.class);

    public TrackDeleteImpl() {
        setAccessPermit(List.of(Role.ADMIN));
    }

    @Override
    public RedirectionInfo execute(final HttpServletRequest request,
                                   final HttpServletResponse response)
            throws IntrusiveException {
        TrackService trackService = manager.createService(ServiceType.TRACK);
        Integer id = Integer.valueOf(request.getParameter("id"));
        if (trackService.isOwned(id)) {
            RedirectionInfo info = new RedirectionInfo("/track/list.html");
            info.setAttribute("message", "You can not delete this track");
            return info;
        } else {
            trackService.delete(id);
            LOG.info(String.format("Track id=%d was deleted", id));
            return new RedirectionInfo("/track/list.html");
        }
    }
}
