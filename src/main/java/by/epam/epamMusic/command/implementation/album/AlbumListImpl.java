package by.epam.epamMusic.command.implementation.album;

import by.epam.epamMusic.command.Command;
import by.epam.epamMusic.command.RedirectionInfo;
import by.epam.epamMusic.entity.Album;
import by.epam.epamMusic.entity.Performer;
import by.epam.epamMusic.entity.Role;
import by.epam.epamMusic.entity.User;
import by.epam.epamMusic.exception.IntrusiveException;
import by.epam.epamMusic.service.AlbumService;
import by.epam.epamMusic.service.PerformerService;
import by.epam.epamMusic.service.ServiceType;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class AlbumListImpl extends Command {
    private final static Integer itemsPerPage = 10;

    public AlbumListImpl() {
        setAccessPermit(List.of(Role.ADMIN, Role.USER));
    }

    @Override
    public RedirectionInfo execute(final HttpServletRequest request,
                                   final HttpServletResponse response)
            throws IntrusiveException {
        AlbumService albumService = manager.createService(ServiceType.ALBUM);
        PerformerService performerService = manager.createService(ServiceType.PERFORMER);
        User user = (User) request.getSession().getAttribute("user");
        List<Album> albums;
        if (request.getParameter("id") != null) {
            Integer id = Integer.valueOf(request.getParameter("id"));
            albums = albumService.findByPerformerId(id);
        } else {
            albums = albumService.readAllAlbums();
        }
        if (user.getRole().equals(Role.ADMIN)) {
            List<Performer> performers = performerService.readAllPerformers();
            request.setAttribute("performers", performers);
        }
        Integer numberOfPages = (albums.size() / itemsPerPage) + 1;
        if (request.getParameter("page") != null) {
            Integer currentPage = Integer.valueOf(request.getParameter("page"));
            request.getSession().setAttribute("currentPage", currentPage);
            request.setAttribute("currentPage", currentPage);
            request.setAttribute("numberOfPages", numberOfPages);
            request.setAttribute("itemsPerPage", itemsPerPage);
        } else {
            Integer currentPage = (Integer) request.getSession().getAttribute("currentPage");
            request.getSession().removeAttribute("currentPage");
            request.getSession().setAttribute("currentPage", currentPage);
            request.setAttribute("numberOfPages", numberOfPages);
            request.setAttribute("itemsPerPage", itemsPerPage);
        }
        request.setAttribute("albums", albums);
        return new RedirectionInfo("/jsp/album/list.jsp");
    }
}
