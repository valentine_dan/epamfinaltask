package by.epam.epamMusic.command.implementation.user;

import by.epam.epamMusic.command.Command;
import by.epam.epamMusic.command.RedirectionInfo;
import by.epam.epamMusic.entity.BankAccount;
import by.epam.epamMusic.entity.Role;
import by.epam.epamMusic.entity.User;
import by.epam.epamMusic.exception.IntrusiveException;
import by.epam.epamMusic.service.BankAccountService;
import by.epam.epamMusic.service.ServiceType;
import by.epam.epamMusic.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class PaymentMethodAddImpl extends Command {
    private static Logger LOG = LogManager.getLogger(PaymentMethodAddImpl.class);

    public PaymentMethodAddImpl() {
        setAccessPermit(List.of(Role.ADMIN, Role.USER));
    }

    @Override
    public RedirectionInfo execute(final HttpServletRequest request,
                                   final HttpServletResponse response)
            throws IntrusiveException {
        BankAccountService bankAccountService = manager.createService(ServiceType.BANK_ACCOUNT);
        UserService userService = manager.createService(ServiceType.USER);
        Integer bankAccountNumber = Integer.valueOf(request.getParameter("number"));
        BankAccount bankAccount = bankAccountService.findByNumber(bankAccountNumber);
        if (bankAccount != null) {
            User userInSession = (User) request.getSession().getAttribute("user");
            userInSession.setBankAccount(bankAccount);
            request.getSession().setAttribute("user", userInSession);
            userService.addPaymentMethod(userInSession);
            LOG.info(String.format("User %s successfully added payment method", userInSession.getLogin()));
            return new RedirectionInfo("/user/profile.html");
        } else {
            RedirectionInfo info = new RedirectionInfo("/user/profile.html");
            info.setAttribute("message", "You've entered number of a nonexistent bank account");
            return info;
        }
    }
}
