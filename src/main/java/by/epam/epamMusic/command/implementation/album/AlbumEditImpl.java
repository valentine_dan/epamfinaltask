package by.epam.epamMusic.command.implementation.album;

import by.epam.epamMusic.command.Command;
import by.epam.epamMusic.command.RedirectionInfo;
import by.epam.epamMusic.entity.Role;
import by.epam.epamMusic.exception.IntrusiveException;
import by.epam.epamMusic.service.AlbumService;
import by.epam.epamMusic.service.PerformerService;
import by.epam.epamMusic.service.ServiceType;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class AlbumEditImpl extends Command {
    public AlbumEditImpl() {
        setAccessPermit(List.of(Role.ADMIN));
    }

    @Override
    public RedirectionInfo execute(final HttpServletRequest request,
                                   final HttpServletResponse response)
            throws IntrusiveException {
        AlbumService service = manager.createService(ServiceType.ALBUM);
        PerformerService performerService = manager.createService(ServiceType.PERFORMER);
        Integer albumId = Integer.valueOf(request.getParameter("id"));
        request.setAttribute("album", service.findById(albumId));
        request.setAttribute("performers", performerService.readAllPerformers());
        return new RedirectionInfo("/jsp/album/edit.jsp");
    }
}
