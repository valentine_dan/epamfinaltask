package by.epam.epamMusic.command.implementation.album;

import by.epam.epamMusic.command.Command;
import by.epam.epamMusic.command.RedirectionInfo;
import by.epam.epamMusic.entity.Album;
import by.epam.epamMusic.entity.Role;
import by.epam.epamMusic.entity.Track;
import by.epam.epamMusic.entity.User;
import by.epam.epamMusic.exception.IntrusiveException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class AlbumRemoveFromCartImpl extends Command {
    public AlbumRemoveFromCartImpl() {
        setAccessPermit(List.of(Role.ADMIN, Role.USER));
    }

    @Override
    public RedirectionInfo execute(final HttpServletRequest request,
                                   final HttpServletResponse response)
            throws IntrusiveException {
        User user = (User) request.getSession().getAttribute("user");
        Set<Track> tracks = user.getCart().getTracks();
        Set<Album> albums = user.getCart().getAlbums();
        Integer albumId = Integer.valueOf(request.getParameter("id"));

        BigDecimal newTotalPrice = new BigDecimal(0);
        tracks = tracks.stream().filter(track
                -> !track.getAlbum().getId().equals(albumId)).
                collect(Collectors.toSet());
        for (Track track :
                tracks) {
            newTotalPrice = newTotalPrice.add(track.getPrice());
        }
        albums = albums.stream().filter(album
                -> !album.getId().equals(albumId)).collect(Collectors.toSet());

        user.getCart().setTracks(tracks);
        user.getCart().setAlbums(albums);
        user.getCart().setTotalPrice(newTotalPrice);
        return new RedirectionInfo("/user/cart.html");
    }
}
