package by.epam.epamMusic.command.implementation.user;

import by.epam.epamMusic.command.Command;
import by.epam.epamMusic.command.RedirectionInfo;
import by.epam.epamMusic.entity.Role;
import by.epam.epamMusic.entity.User;
import by.epam.epamMusic.exception.IntrusiveException;
import by.epam.epamMusic.service.ServiceType;
import by.epam.epamMusic.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class UserSignInImpl extends Command {
    private static Logger LOG = LogManager.getLogger(UserSignInImpl.class);


    public UserSignInImpl() {
        setAccessPermit(List.of(Role.ADMIN, Role.USER));
    }

    @Override
    public RedirectionInfo execute(final HttpServletRequest request,
                                   final HttpServletResponse response)
            throws IntrusiveException {
        UserService userService = manager.createService(ServiceType.USER);
        HttpSession session = request.getSession();
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        User user = userService.findByLoginAndPassword(login, password);

        if (user != null) {
            RedirectionInfo info = new RedirectionInfo("/user/index.html");
            session.setAttribute("user", user);
            request.removeAttribute("message");
            LOG.info(String.format("User %s successfully signed in", user.getLogin()));
            return info;
        } else {
            RedirectionInfo info = new RedirectionInfo("/signIn.jsp");
            request.setAttribute("message", "You've entered invalid login or password");
            return info;
        }
    }
}
