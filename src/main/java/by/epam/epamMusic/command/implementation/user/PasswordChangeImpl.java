package by.epam.epamMusic.command.implementation.user;

import by.epam.epamMusic.command.Command;
import by.epam.epamMusic.command.RedirectionInfo;
import by.epam.epamMusic.entity.Role;
import by.epam.epamMusic.entity.User;
import by.epam.epamMusic.exception.IntrusiveException;
import by.epam.epamMusic.service.ServiceType;
import by.epam.epamMusic.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class PasswordChangeImpl extends Command {
    private static Logger LOG = LogManager.getLogger(PasswordChangeImpl.class);

    public PasswordChangeImpl() {
        setAccessPermit(List.of(Role.ADMIN, Role.USER));
    }

    @Override
    public RedirectionInfo execute(final HttpServletRequest request,
                                   final HttpServletResponse response)
            throws IntrusiveException {
        UserService userService = manager.createService(ServiceType.USER);
        String oldPassword = request.getParameter("old-password");
        String newPassword = request.getParameter("newPass1");
        User userInSession = (User) request.getSession().getAttribute("user");
        User user = userService.findByLoginAndPassword(userInSession.getLogin(), oldPassword);
        RedirectionInfo info = new RedirectionInfo("/user/profile.html");
        if (oldPassword != null && newPassword != null) {
            if (user != null) {
                user.setPassword(newPassword);
                userService.update(user);
                userInSession = userService.findByLoginAndPassword(userInSession.getLogin(), newPassword);
                request.getSession().setAttribute("user", userInSession);
                info.setAttribute("message", "Password was successfully changed!");
                LOG.info(String.format("User %s successfully changed password", user.getLogin()));
                return info;
            } else {
                info.setAttribute("message", "You've entered invalid old password");
                return info;
            }
        } else {
            info.setAttribute("message", "Invalid password");
            return info;
        }


    }
}
