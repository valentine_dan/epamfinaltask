package by.epam.epamMusic.command.implementation.user;

import by.epam.epamMusic.command.Command;
import by.epam.epamMusic.command.RedirectionInfo;
import by.epam.epamMusic.entity.Bonus;
import by.epam.epamMusic.entity.Role;
import by.epam.epamMusic.entity.Track;
import by.epam.epamMusic.entity.User;
import by.epam.epamMusic.exception.IntrusiveException;
import by.epam.epamMusic.service.BonusService;
import by.epam.epamMusic.service.ServiceType;
import by.epam.epamMusic.service.TrackService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.List;

public class TrackGetFreeImpl extends Command {
    public TrackGetFreeImpl() {
        setAccessPermit(List.of(Role.ADMIN, Role.USER));
    }

    @Override
    public RedirectionInfo execute(final HttpServletRequest request,
                                   final HttpServletResponse response)
            throws IntrusiveException {
        TrackService service = manager.createService(ServiceType.TRACK);
        BonusService bonusService = manager.createService(ServiceType.BONUS);
        Bonus bonus = bonusService.readByType(Bonus.Type.FREE_TRACK).get(0);
        User user = (User) request.getSession().getAttribute("user");
        User.Cart cart = user.getCart();
        Integer trackId = Integer.valueOf(request.getParameter("id"));
        Track track = service.findById(trackId);
        cart.getTracks().remove(track);
        BigDecimal newTotalPrice = cart.getTotalPrice().subtract(track.getPrice());
        bonusService.writeOff(user.getId(), bonus.getId());
        cart.setTotalPrice(newTotalPrice);
        user.setCart(cart);
        return new RedirectionInfo("/user/cart.html");
    }
}
