package by.epam.epamMusic.command.implementation.user;

import by.epam.epamMusic.command.Command;
import by.epam.epamMusic.command.RedirectionInfo;
import by.epam.epamMusic.entity.Role;
import by.epam.epamMusic.entity.User;
import by.epam.epamMusic.exception.IntrusiveException;
import by.epam.epamMusic.service.ServiceType;
import by.epam.epamMusic.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class UserListImpl extends Command {
    public UserListImpl() {
        setAccessPermit(List.of(Role.ADMIN));
    }

    @Override
    public RedirectionInfo execute(final HttpServletRequest request,
                                   final HttpServletResponse response)
            throws IntrusiveException {
        UserService service = manager.createService(ServiceType.USER);
        List<User> users = service.readAllUsers();
        request.setAttribute("users", users);
        return new RedirectionInfo("/jsp/user/list.jsp");
    }
}
