package by.epam.epamMusic.command.implementation.track;

import by.epam.epamMusic.command.Command;
import by.epam.epamMusic.command.RedirectionInfo;
import by.epam.epamMusic.entity.Role;
import by.epam.epamMusic.exception.IntrusiveException;
import by.epam.epamMusic.exception.InvalidDataException;
import by.epam.epamMusic.service.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

public class TrackAddImpl extends Command {
    private static Logger LOG = LogManager.getLogger(TrackAddImpl.class);

    public TrackAddImpl() {
        setAccessPermit(List.of(Role.ADMIN));
    }

    @Override
    public RedirectionInfo execute(final HttpServletRequest request,
                                   final HttpServletResponse response)
            throws IntrusiveException {
        try {
            TrackService service = manager.createService(ServiceType.TRACK);
            String trackName = request.getParameter("trackName");
            BigDecimal trackPrice = new BigDecimal(request.getParameter("trackPrice"));
            Integer albumId = Integer.valueOf(request.getParameter("album"));
            service.add(trackName, trackPrice, albumId);
            LOG.info("New track was added");
            return new RedirectionInfo("/track/list.html");
        } catch (InvalidDataException e) {
            request.setAttribute("message", Arrays.toString(e.getStackTrace()));
            return new RedirectionInfo("/error.jsp");
        }
    }
}
