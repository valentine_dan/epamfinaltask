package by.epam.epamMusic.command.implementation.user;

import by.epam.epamMusic.command.Command;
import by.epam.epamMusic.command.RedirectionInfo;
import by.epam.epamMusic.entity.Role;
import by.epam.epamMusic.exception.IntrusiveException;
import by.epam.epamMusic.service.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class LoadMediaLibImpl extends Command {
    public LoadMediaLibImpl() {
        setAccessPermit(List.of(Role.ADMIN, Role.USER));
    }

    @Override
    public RedirectionInfo execute(final HttpServletRequest request, final HttpServletResponse response)
            throws IntrusiveException {
        AlbumService albumService = manager.createService(ServiceType.ALBUM);
        PerformerService performerService = manager.createService(ServiceType.PERFORMER);
        TrackService trackService = manager.createService(ServiceType.TRACK);
        request.setAttribute("tracks", trackService.readAllTracks());
        request.setAttribute("albums", albumService.readAllAlbums());
        request.setAttribute("performers", performerService.readAllPerformers());
        return new RedirectionInfo("/jsp/user/index.jsp");
    }
}
