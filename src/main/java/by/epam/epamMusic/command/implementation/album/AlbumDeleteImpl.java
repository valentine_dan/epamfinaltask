package by.epam.epamMusic.command.implementation.album;

import by.epam.epamMusic.command.Command;
import by.epam.epamMusic.command.RedirectionInfo;
import by.epam.epamMusic.entity.Album;
import by.epam.epamMusic.entity.Role;
import by.epam.epamMusic.entity.Track;
import by.epam.epamMusic.exception.IntrusiveException;
import by.epam.epamMusic.service.AlbumService;
import by.epam.epamMusic.service.ServiceType;
import by.epam.epamMusic.service.TrackService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class AlbumDeleteImpl extends Command {
    private static Logger LOG = LogManager.getLogger(AlbumDeleteImpl.class);

    public AlbumDeleteImpl() {
        setAccessPermit(List.of(Role.ADMIN));
    }

    @Override
    public RedirectionInfo execute(final HttpServletRequest request,
                                   final HttpServletResponse response)
            throws IntrusiveException {
        RedirectionInfo info = new RedirectionInfo("/album/list.html");
        AlbumService service = manager.createService(ServiceType.ALBUM);
        TrackService trackService = manager.createService(ServiceType.TRACK);
        Integer id = Integer.valueOf(request.getParameter("id"));
        List<Track> tracks = trackService.findByAlbumId(id);
        if (!tracks.isEmpty()) {
            Album album = service.findById(id);
            info.setAttribute("message", "You can't delete album '" + album.getName() + "'");
            return info;
        } else {
            service.delete(id);
            LOG.info(String.format("Album id=%d was deleted", id));
            return info;
        }
    }
}
