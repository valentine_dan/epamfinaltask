package by.epam.epamMusic.command.implementation;

import by.epam.epamMusic.command.Command;
import by.epam.epamMusic.command.RedirectionInfo;
import by.epam.epamMusic.entity.Bonus;
import by.epam.epamMusic.entity.Role;
import by.epam.epamMusic.exception.IntrusiveException;
import by.epam.epamMusic.service.BonusService;
import by.epam.epamMusic.service.ServiceType;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class AdminFreeTrackAddImpl extends Command {
    public AdminFreeTrackAddImpl() {
        setAccessPermit(List.of(Role.ADMIN));
    }

    @Override
    public RedirectionInfo execute(final HttpServletRequest request,
                                   final HttpServletResponse response)
            throws IntrusiveException {
        BonusService bonusService = manager.createService(ServiceType.BONUS);
        List<Bonus> bonuses = bonusService.readByType(Bonus.Type.FREE_TRACK);
        if (bonuses.isEmpty()) {
            bonusService.add(100., Bonus.Type.FREE_TRACK);
        }
        bonuses = bonusService.readByType(Bonus.Type.FREE_TRACK);
        Bonus bonus = bonuses.get(0);
        Integer userId = Integer.valueOf(request.getParameter("userId"));
        bonusService.addToUser(bonus, userId);
        return new RedirectionInfo("/user/list.html");
    }
}
