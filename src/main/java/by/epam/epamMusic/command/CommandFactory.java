package by.epam.epamMusic.command;

import by.epam.epamMusic.command.implementation.*;
import by.epam.epamMusic.command.implementation.album.*;
import by.epam.epamMusic.command.implementation.performer.*;
import by.epam.epamMusic.command.implementation.track.*;
import by.epam.epamMusic.command.implementation.user.*;
import by.epam.epamMusic.exception.IntrusiveException;
import by.epam.epamMusic.service.AbstractServiceManager;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public final class CommandFactory {
    private static final Map<CommandType, Class<? extends Command>> commands = new HashMap<>();
    private AbstractServiceManager serviceManager;

    static {
        commands.put(CommandType.DEFAULT, DefaultImpl.class);
        commands.put(CommandType.SIGN_IN, UserSignInImpl.class);
        commands.put(CommandType.SIGN_UP, UserSignUpImpl.class);
        commands.put(CommandType.SIGN_OUT, UserSignOutImpl.class);

        commands.put(CommandType.USER_LIST, UserListImpl.class);
        commands.put(CommandType.TRACK_LIST, TrackListImpl.class);
        commands.put(CommandType.ALBUM_LIST, AlbumListImpl.class);
        commands.put(CommandType.PERFORMER_LIST, PerformerListImpl.class);

        commands.put(CommandType.PERFORMER_ADD, PerformerAddImpl.class);
        commands.put(CommandType.TRACK_ADD, TrackAddImpl.class);
        commands.put(CommandType.ALBUM_ADD, AlbumAddImpl.class);

        commands.put(CommandType.TRACK_EDIT, TrackEditImpl.class);
        commands.put(CommandType.TRACK_SAVE, TrackSaveImpl.class);
        commands.put(CommandType.TRACK_ADD_TO_CART, TrackAddToCart.class);
        commands.put(CommandType.TRACK_REMOVE_FROM_CART, TrackRemoveFromCartImpl.class);
        commands.put(CommandType.TRACK_DELETE, TrackDeleteImpl.class);

        commands.put(CommandType.PERFORMER_EDIT, PerformerEditImpl.class);
        commands.put(CommandType.PERFORMER_SAVE, PerformerSaveImpl.class);
        commands.put(CommandType.PERFORMER_DELETE, PerformerDeleteImpl.class);

        commands.put(CommandType.ALBUM_EDIT, AlbumEditImpl.class);
        commands.put(CommandType.ALBUM_SAVE, AlbumSaveImpl.class);
        commands.put(CommandType.ALBUM_DELETE, AlbumDeleteImpl.class);
        commands.put(CommandType.ALBUM_ADD_TO_CART, AlbumAddToCartImpl.class);
        commands.put(CommandType.ALBUM_REMOVE_FROM_CART, AlbumRemoveFromCartImpl.class);

        commands.put(CommandType.LOAD_MEDIA, LoadMediaLibImpl.class);
        commands.put(CommandType.PASSWORD_CHANGE, PasswordChangeImpl.class);
        commands.put(CommandType.EMAIL_CHANGE, EmailChangeImpl.class);
        commands.put(CommandType.PROFILE, UserProfileImpl.class);
        commands.put(CommandType.ADD_PAYMENT_METHOD, PaymentMethodAddImpl.class);
        commands.put(CommandType.BALANCE_TOP_UP, BalanceTopUpImpl.class);
        commands.put(CommandType.CART, UserCartImpl.class);
        commands.put(CommandType.BUY, MusicBuyImpl.class);

        commands.put(CommandType.DISCOUNT_ADD, AdminDiscountAddImpl.class);
        commands.put(CommandType.FREE_TRACK_ADD, AdminFreeTrackAddImpl.class);
        commands.put(CommandType.FREE_TRACK_GET, TrackGetFreeImpl.class);
    }

    public void setServiceManager(AbstractServiceManager serviceManager) {
        this.serviceManager = serviceManager;
    }

    public <Type extends Command> Type getCommand(CommandType type) throws IntrusiveException {
        Class<? extends Command> cl = commands.get(type);
        try {
            @SuppressWarnings("unchecked")
            Type command = (Type) cl.getDeclaredConstructor().newInstance();
            command.setManager(serviceManager);
            return command;
        } catch (InstantiationException
                | IllegalAccessException
                | InvocationTargetException
                | NoSuchMethodException e) {
            throw new IntrusiveException(e);
        }
    }

    public void close() {
        serviceManager.close();
    }
}
