package by.epam.epamMusic.command;

import java.util.HashMap;
import java.util.Map;

public class RedirectionInfo {
    private String url;
    private Map<String, Object> attributes = new HashMap<>();

    public RedirectionInfo(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Map<String, Object> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<String, Object> attributes) {
        this.attributes = attributes;
    }

    public void setAttribute(String key, Object value){
        attributes.put(key, value);
    }

    public Object getAttribute(String key){
        return attributes.get(key);
    }

    public void removeAttribute(String key){
        attributes.remove(key);
    }
}
