import by.epam.epamMusic.dao.ConnectionPool;
import by.epam.epamMusic.dao.ProxyConnection;
import by.epam.epamMusic.exception.IntrusiveException;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.sql.SQLException;

public class ConnectionPoolTest {
    private static ConnectionPool instance;

    @BeforeClass
    public static void init() {
        instance = ConnectionPool.getInstance();
    }

    @Test
    public void obtainConnectionTest() throws IntrusiveException {
        for (int i = 0; i < 20; i++) {
            ProxyConnection connection = instance.obtainConnection();
            Assert.assertNotNull(connection);
        }
    }

    @AfterClass
    public static void freePool(){
        instance.free();
    }

}
